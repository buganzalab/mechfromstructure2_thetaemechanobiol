/*

RESULTS
circular wound problem, solved by modeling 1/4 of it and using X and Y symmetry.

Read a tri mesh defined from Abaqus.
Then apply boundary conditions and initial conditions according to in vivo wounding.
Solve.

Code implementation based on 2017 Adrian Buganza-Tepole's paper on CMAME

*/

//
#include "wound_tri.h"
#include "utilities_tri.h"
#include "solver_tri.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include <math.h>
#include <string>
#include <time.h>
#include <stdlib.h>
//#include<Eigen/StdVector> // to deal with std::vector<Vector2d>

#include <Eigen/Dense>
using namespace Eigen;


double frand(double fMin, double fMax)
{
    //double f = (double)rand() / RAND_MAX;
    //return fMin + f * (fMax - fMin);
    return 0.5;
}


int main(int argc, char *argv[])
{

	// ARGV
	// argv[1] mesh 
	// argv[2] input
	// argv[3] output

	std::cout<<"\nResults: symmetric domain simulations\n";
	srand (time(NULL));
	//
	//---------------------------------//
	// DEFINE ELLIPTICAL WOUND GEOMETRY
	double x_center      = 0.; 		//[mm] wound center x coord
	double y_center      = 0.; 		//[mm] wound center y coord
	double x_axis        = 2.5; 	//[mm] semi-axis length along x
	double y_axis        = 2.5;		//[mm] semi-axis length along y
	double alpha_ellipse = 0.;  	//[-]  angle for ellipse (circular -> 0)
	double domain_Lx     = 25.;	 	//[mm] domain dimension along x
	double domain_Ly     = 25.;	 	//[mm] domain dimension along y
	//
	//---------------------------------//
	// DEFINE BOUNDARY CONDITIONS SIMULATION DOMAIN
	// NOTE: stretch = 1 is NOT unconstrained
	double x_stretch     = 1.15; 	//[-] imposed tissue stretch along x
	double y_stretch     = 1.15;    //[-] imposed tissue stretch along y
	double x_displ       = (x_stretch-1.)*domain_Lx;
	double y_displ       = (y_stretch-1.)*domain_Ly;
	//
	//---------------------------------//
	// DEFINE SKIN CONSTITUTIVE MODEL (GOH hyperelastic: isotropic, incompressible)
	double thick_0       = 1.7;		//[mm] tissue thickness
	double mu0_healthy   = 0.024;  	//[MPa] GOH model mu0 for normal skin (this is 2x the C10 used in abaqus)
	double kc0_healthy   = 1.6;   	//[MPa] GOH model k1 for collagen fiber family in normal skin
	double k2c           = 0.88;	//[-] GOH model k2 for collagen fiber family in normal skin
	double kappac0_healthy = 1./3.; //[-] GOH model kappa for collagen fiber family in normal skin (assumed isotropic)
	double kf0_healthy   = 1.; 		//[-] in this model we use kf to represent the collagen crosslinking \xi_c, which has healthy value of 1
	double k2f           = 0.;		//[-] GOH model k2 for fibronectin fiber family in normal skin (not modeled -> 0)
	double kappaf0_healthy = 0.;    //[-] GOH model kappa for fibronectin fiber family in normal skin (not modeled -> 0)
	//
	//---------------------------------//
	// DEFINE INITIAL CONDITIONS NORMAL SKIN (c, rho, phi, lambdaP)
	double prec_healthy  = 0.; 		//[-] normalized value for healthy content of early inflammatory signal
	double c_healthy     = 1.;  	//[-] normalized content of chemokine in healthy tissue
	double rho_healthy   = 1.; 		//[-] normalized value for healthy cell content
	double phic0_healthy = 1.; 		//[-] normalized healthy value for collagen fraction
	double phif0_healthy = 0.; 		//[-] normalized healthy value for fibronectin fraction (not modeled)
	Vector2d a0c_healthy;a0c_healthy<<1.0,0.0;
	Vector2d a0f_healthy;a0f_healthy<<1.0,0.0;
	Vector2d lamda0_healthy;lamda0_healthy<<1.,1.;
	//
	//---------------------------------//
	// DEFINE GLOBAL PARAMETERS FOR NORMAL AND WOUNDED SKIN
	//
	// precursor parameters
	double D_precprec    = 0.075; 	//[mm^2/h] diffusion of precursor, same as D_cc
	double d_prec        = 0.01;    //[1/h] decay of precursor, same as d_c
	//
	// chemical species parameters
	double D_cc          = 0.075;   //[mm^2/h] diffusion of chemokines, from reference in CMAME paper
	double p_c_prec      = 0.23;    //[1/h] production of chemokines in consequence of precursor, selected to match peak in c
	double p_c_rho       = 1.5;     //[1/h] production of chemokines by cells, from Li et al., Curr Eye Res 19, 1999
	double p_c_theta     = 0;       //[1/h] coupling of mechanics and chemokines not modeled, hence set to 0
	double K_c_c         = 0.5;     // saturation of c by c, assumed
	double d_c           = 0.01;    //[1/h] decay of chemokines, from reference in CMAME paper
	// impose value for target content of chemokines in order to ensure homeostasis at physiological stretch
	double c_target = 1.0 + d_c * (1.0 + K_c_c) / p_c_rho - p_c_theta * 0.5 / p_c_rho;
	//
	// cell (fibroblast) parameters
	double D_rhorho      = 0.035;   //[mm^2/h] diffusion of cells, from reference in CMAME paper
	double D_rhoc        = 8e-5;    //[mm^2/h] diffusion of chemotactic gradient, from reference in CMAME paper
	double p_rho         = 0.034;   //[1/h] natural cell proliferation rate, from reference in CMAME paper (based on commercial fibroblast doubling rate data)
	double p_rho_c       = 5.0 * p_rho; //[1/h] enhancement of cell production production due to c, selected to match peak in rho (together with K_rho_c and K_rho_rho)
	double K_rho_c       = 10.;     // saturation of cell proliferation by c, selected to match peak in rho (together with p_rho_c and K_rho_rho)
	double p_rho_theta   = 0.01 * p_rho; //[1/h] enhancement of cell production by stretch, selected to match data from Yang et al., J Biomech 37, 2004
	double K_rho_rho     = 30.;     // saturation of cell proliferation by cells, selected to match peak in rho (together with p_rho_c and K_rho_c)
	// impose value for cell decay in order to ensure homeostasis at physiological stretch
	double d_rho = (p_rho + p_rho_c / (1.0 + K_rho_c) + p_rho_theta * 0.5 ) * (1.0 - 1.0 / K_rho_rho);
	// logistic function for Michaelis�Menten kinetics
	double gamma_theta   = 5.;      // sensitivity of heaviside function, same as in CMAME paper
	double vartheta_e = x_stretch * y_stretch; // physiological state of area stretch (equibiaxial for simplicity)
	// Active stress exerted by fibroblasts
	double K_t_c         = 1e-5;    // saturation of force by c, same as in CMAME paper
	double Fc_max        = 0.;      // effect of the chemical concentration on the chemotactic response (not used -> 0)
	double t_rho         = 0.0405;  //[MPa/mm^3] force of fibroblasts, value from CMAME paper (x number of cells) -> overwritten after reading input.txt file
	double t_rho_c       = 0.0405;  //[MPa/mm^3] force of myofibroblasts as enhanced by c, value from CMAME paper (x number of cells) -> overwritten after reading input.txt file
	//
	//---------------------------------//
	std::vector<double> global_parameters = {mu0_healthy, k2c, k2f, t_rho, t_rho_c, K_t_c, D_rhorho, D_rhoc, Fc_max, D_cc, p_rho, p_rho_c, p_rho_theta, K_rho_c, K_rho_rho, d_rho, vartheta_e, gamma_theta, p_c_rho, p_c_theta, K_c_c, d_c, thick_0, D_precprec, d_prec, p_c_prec, prec_healthy, c_target, rho_healthy};
	//
	//---------------------------------//
	// DEFINE LOCAL PARAMETERS FOR WOUNDED SKIN
	//
	// initialize variables for wound (will be overwritten when reading solver input file)
	double prec_wound    = 0.;
	double rho_wound     = 0.;
	double c_wound       = 0.;
	double phic0_wound   = 0.;
	double mu0_wound     = 0.;
	double kc0_wound     = 0.;
	double phif0_wound   = 0.;
	double kf0_wound     = 0.;
	double kappac0_wound = 0.;
	double kappaf0_wound = 0.;
	double a0y = frand(0.9,1.);
	double a0x = sqrt(1-a0y*a0y);
	Vector2d a0c_wound;a0c_wound<<1.0,0.0;
	Vector2d a0f_wound;a0f_wound<<1.0,0.0;
	Vector2d lamda0_wound;lamda0_wound<<1.,1.;
	//
	// fiber alignment (not modeled)
	double tau_omega_c   = 1e10;   // time constant for angular reorientation of collagen (not modeled -> set to very large value)
	double tau_omega_f   = 1e10;   // time constant for angular reorientation of fibronectin (not modeled -> set to very large value)
	//
	// dispersion parameter (not modeled)
	double tau_kappa_c   = 1e10;   // time constant
	double gamma_kappa_c = 5;      // exponent of the principal stretch ratio
	double tau_kappa_f   = 1e10;   // time constant
	double gamma_kappa_f = 5;      // exponent of the principal stretch ratio
	//
	// permanent contracture/growth
	double tau_lamdaP_a  = 0.485;  // time constant for direction a, same as in CMAME paper
	double tau_lamdaP_s  = 0.485;  // time constant for direction s, same as in CMAME paper
	//
	// volume fractions of collagen and fibronectin (not modeled)
	double phic_vol      = 1e-16;  // volume of collagen associated to healthy collagen volume fraction (not modeled -> set to very small value)
	double phif_vol      = 1e-16;  // volume ot fibronectin associated to healthy fibronectin volume fraction (not modeled -> set to very small value)
	//
	// collagen deposition parameters
	double p_phi         = 2e-3;   //[1/h] collagen production by fibroblasts, ~5% per day
	double p_phi_c       = 2.5*p_phi; //[1/h] upregulation of fibroblast production in response to c and rho, selected to match peak in phic
	double p_phi_theta   = 0.01*p_phi;//[1/h] upregulation of fibroblast production in response to stretch, assumed p_phi_theta/p_phi = p_rho_theta/p_rho
	double K_phi_c       = 1e-4;   // saturation of phic by c, same as in CMAME paper
	double K_phi_rho     = 1.06;   // saturation of phic by phic, same as in CMAME paper
	double d_phi_rho_c   = 4.85e-4;//[1/h] phic degradation coupled to c and rho, same value as in CMAME paper
	// impose value for rate of degradation in order to ensure homeostasis at physiological stretch
	double d_phi = ( p_phi + p_phi_c / ( 1. + K_phi_c ) + p_phi_theta * 0.5 ) / ( 1. + K_phi_rho ) - d_phi_rho_c;
	//
	// collagen crosslinking parameters
	double p_csi_phic = 1.;   //[1/h] natural rate of collagen crosslink formation, assumed
	double K_csi_phic = 0.5;  // saturation of crosslink production based on collagen content, assumed
	double d_csi_phic_m = 1.; //[1/h] degradation of collagen crosslinks due to collagen depletion (phic_minus), assumed
	//
	// fibrin degradation parameters in the wound
	double d_phif = d_phi;     //[1/h] natural rate of fibrin degradation, assumed equal to d_phi
	double d_phif_c = 0.00325; //[1/h] fibrin degradation enhanced by c and rho, selected to match experimental data for C10
	//
	std::vector<double> local_parameters = {tau_omega_c, tau_omega_f, tau_kappa_c, gamma_kappa_c, tau_kappa_f, gamma_kappa_f, tau_lamdaP_a, tau_lamdaP_s, thick_0, phic_vol, phif_vol, p_phi, p_phi_c, K_phi_c, p_phi_theta, K_phi_rho, d_phi, d_phi_rho_c, vartheta_e, gamma_theta, p_csi_phic, K_csi_phic, d_csi_phic_m, d_phif, d_phif_c};
	//
	//
	// other local stuff
	double PIE = 3.14159;
	//
	//---------------------------------//
	// read values from solver input file
	std::ifstream myfile(argv[2]);
	std::string line;
	double time_final, time_step;
	int max_iter,max_time_step_doublings;
	double freq;
	double load_step;
	int max_load_step_doublings;
	if (myfile.is_open())
	{
		// time final
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss0(line);
		ss0>>time_final;
		// time step
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss1(line);
		ss1>>time_step;
		// max iter
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss2(line);
		ss2>>max_iter;
		// max time step doublings
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss3(line);
		ss3>>max_time_step_doublings;
		// load step
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss1L(line);
		ss1L>>load_step;
		// max load step doublings
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss3L(line);
		ss3L>>max_load_step_doublings;
		// save freq
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss4(line);
		ss4>>freq;
		// prec_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss5a(line);
		ss5a>>prec_wound;
		// rho_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss5(line);
		ss5>>rho_wound;
		// c_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss6(line);
		ss6>>c_wound;
		// phic0_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss7(line);
		ss7>>phic0_wound;
		// mu0_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss8(line);
		ss8>>mu0_wound;
		// kc0_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss9(line);
		ss9>>kc0_wound;
		// phif0 wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss10(line);
		ss10>>phif0_wound;
		// kf0_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss11(line);
		ss11>>kf0_wound;
		// kappac0_wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss12(line);
		ss12>>kappac0_wound;
		// kappaf0 wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss13(line);
		ss13>>kappaf0_wound;
		// a0x wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss14(line);
		ss14>>a0x;
		// a0y wound
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss15(line);
		ss15>>a0y;
		a0c_wound(0) = a0x;
		a0c_wound(1) = a0y;
		a0f_wound(0) = a0x;
		a0f_wound(1) = a0y;
		// trho
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss16(line);
		ss16>>t_rho;
		global_parameters[3] = t_rho;
		// trho_c
		getline (myfile,line);
		getline (myfile,line);
		std::stringstream ss17(line);
		ss17>>t_rho_c;
		global_parameters[4] = t_rho_c;
	}
	myfile.close();


	//---------------------------------//
	// create mesh (only nodes and elements)
	std::cout<<"Going to read the mesh from file: "<<argv[1]<<"\n";
	tissue myTissue;
	readComsolEditInput(argv[1],myTissue);
	std::cout<<"Read in mesh with: "<<myTissue.n_node<<" nodes, "<<myTissue.n_tri<<" elements\n";
	int n_node = myTissue.n_node;
	int n_elem = myTissue.n_tri;
	// and initialize the rest of the fields in the struct and send to run a single step
	//
	// global fields rho and c initial conditions
	std::vector<double> node_rho0(n_node,rho_healthy);
	std::vector<double> node_c0 (n_node,c_target);
	//
	// values at the integration points
	std::vector<double> ip_phic0(n_elem,phic0_healthy);
	std::vector<double> ip_mu0(n_elem,mu0_healthy);
	std::vector<double> ip_kc0(n_elem,kc0_healthy);
	//std::vector<Vector2d, aligned_allocator<Vector2d> > ip_a0c0(n_elem,a0c_healthy);
	std::vector<Vector2d> ip_a0c0(n_elem,a0c_healthy);
	std::vector<double> ip_kappac0(n_elem,kappac0_healthy);
	//
	std::vector<double> ip_phif0(n_elem,phif0_healthy);
	std::vector<double> ip_kf0(n_elem,kf0_healthy);
	std::vector<Vector2d> ip_a0f0(n_elem,a0f_healthy);
	std::vector<double> ip_kappaf0(n_elem,kappaf0_healthy);
	//
	std::vector<Vector2d> ip_lamda0(n_elem,lamda0_healthy);
	//
	// boundary conditions and definition of the wound
	// 
	double tol_boundary = 0.01;
	std::map<int,double> eBC_x;
	std::map<int,double> eBC_rho;
	std::map<int,double> eBC_c;
	std::vector<int> nodesXSymm;
	std::vector<int> nodesYSymm;
	std::vector<int> nodesXPull;
	std::vector<int> nodesYPull;
	myTissue.node_x = myTissue.node_X;
	for(int nodei=0;nodei<n_node;nodei++){
		double x_coord = myTissue.node_X[nodei](0);
		double y_coord = myTissue.node_X[nodei](1);
		myTissue.node_x[nodei](0)=x_coord*x_stretch;
		myTissue.node_x[nodei](1)=y_coord*y_stretch;
		// Outer edges: check if node is close to the edge of the mesh (adjust tol_boundary as needed!)
		// edge normal to x axis
		if(x_coord > domain_Lx - tol_boundary){
			// boundary condition for displacement
			eBC_x.insert ( std::pair<int,double>(nodei*2+0, x_displ) ); //imposing x displacement
			nodesXPull.push_back(nodei);
			// insert the boundary condition for rho
			eBC_rho.insert ( std::pair<int,double>(nodei, rho_healthy) ); //imposing healthy value far from wound
			// insert the boundary condition for c
			eBC_c.insert   ( std::pair<int,double>(nodei, c_target) ); //imposing healthy value far from wound
		}
		// edge normal to y axis
		if(y_coord > domain_Ly - tol_boundary){
			// boundary condition for displacement
			eBC_x.insert ( std::pair<int,double>(nodei*2+1, y_displ) ); //imposing y displacement
			nodesYPull.push_back(nodei);
			// insert the boundary condition for rho
			eBC_rho.insert ( std::pair<int,double>(nodei, rho_healthy) ); //imposing healthy value far from wound
			// insert the boundary condition for c
			eBC_c.insert   ( std::pair<int,double>(nodei, c_target) ); //imposing healthy value far from wound
		}
		// symmetry on edge at x = 0
		if(x_coord < tol_boundary){
      		eBC_x.insert ( std::pair<int,double>(nodei*2+0, 0.) ); //no x displacement
      		nodesXSymm.push_back(nodei);
    	}
		// symmetry on edge at y = 0
    	if(y_coord < tol_boundary){
      		eBC_x.insert ( std::pair<int,double>(nodei*2+1, 0.) ); //no y displacement
      		nodesYSymm.push_back(nodei);
    	}
	}

	// no neumann boundary conditions.
	std::map<int,double> nBC_x; nBC_x.clear();
	std::map<int,double> nBC_rho; nBC_rho.clear();
	std::map<int,double> nBC_c; nBC_c.clear();

	// parameters
	myTissue.global_parameters = global_parameters;
	myTissue.local_parameters  = local_parameters;
	//
	myTissue.node_rho_0  = node_rho0;
	myTissue.node_rho    = node_rho0;
	myTissue.node_c_0    = node_c0;
	myTissue.node_c      = node_c0;
	//
	myTissue.ip_phic_0   = ip_phic0;
	myTissue.ip_phic     = ip_phic0;
	myTissue.ip_mu_0     = ip_mu0;
	myTissue.ip_kc_0     = ip_kc0;
	myTissue.ip_mu       = ip_mu0;
	myTissue.ip_kc       = ip_kc0;
	myTissue.ip_a0c_0    = ip_a0c0;
	myTissue.ip_a0c      = ip_a0c0;
	myTissue.ip_kappac_0 = ip_kappac0;
	myTissue.ip_kappac   = ip_kappac0;
	//
	myTissue.ip_phif_0   = ip_phif0;
	myTissue.ip_phif     = ip_phif0;
	myTissue.ip_kf_0     = ip_kf0;
	myTissue.ip_kf       = ip_kf0;
	myTissue.ip_a0f_0    = ip_a0f0;
	myTissue.ip_a0f      = ip_a0f0;
	myTissue.ip_kappaf_0 = ip_kappaf0;
	myTissue.ip_kappaf   = ip_kappaf0;
	//
	myTissue.ip_lamdaP_0 = ip_lamda0;
	myTissue.ip_lamdaP   = ip_lamda0;
	//
	// initialize deformation gradient and cauchy stress tensors
	std::vector<Matrix2d> FF(n_elem,Matrix2d::Identity(2,2));
	std::vector<Matrix2d> PK2(n_elem,Matrix2d::Zero(2,2));
	myTissue.ip_FF = FF;
	myTissue.ip_sig_all  = PK2;
	//
	// impose BC
	myTissue.eBC_x       = eBC_x;
	myTissue.eBC_rho     = eBC_rho;
	myTissue.eBC_c       = eBC_c;
	myTissue.nBC_x       = nBC_x;
	myTissue.nBC_rho     = nBC_rho;
	myTissue.nBC_c       = nBC_c;
	//
	// set solver parameters
	myTissue.time_final  = time_final;
	myTissue.time_step   = time_step;
	myTissue.local_time_step = 1.01;
	myTissue.tol         = 1e-9;
	myTissue.max_iter    = max_iter;
	myTissue.max_time_step_doublings = max_time_step_doublings;
	myTissue.load_step   = load_step;
	myTissue.max_load_step_doublings = max_load_step_doublings;
	myTissue.n_IP        = n_elem;
	//
	// Add the precursor even if it is not needed 
	std::vector<double> node_p0 (n_node,0.0);
	std::vector<double> node_p (n_node,0.0);
	myTissue.node_p_0    = node_p0;
	myTissue.node_p      = node_p;
	std::map<int,double> eBC_p;eBC_p.clear();
	std::map<int,double> nBC_p;nBC_p.clear();
	myTissue.eBC_p       = eBC_p;
	myTissue.nBC_p       = nBC_p;

	//
	std::cout<<"\n\nTime solution parameters\nfinal time: "<<myTissue.time_final<<"\ntime step: "<<myTissue.time_step<<"\nmax newton iterations: "<<myTissue.max_iter<<"\nmax time step doublings: "<<myTissue.max_time_step_doublings<<"\n";
	std::cout<<"\n\nLoad solution parameters\nload step: "<<myTissue.load_step<<"\nmax load step doublings: "<<myTissue.max_load_step_doublings<<"\n";
	std::cout<<"save every: "<<freq<<" time\n\n\n";
	//
	std::string filename(argv[3]);

	//----------------------------------------------------------//
	// WOUND HEALING SIMULATION
	// 
	// 1) prestretch unwounded tissue
	// 2) inflict injury and equilibrate mechanics
	// 3) simulate healing time course
	//
	//----------------------------------------------------------//

	//----------------------------------------------------------//
	// 1) PRESTRETCH UNWOUNDED TISSUE TO IN VIVO STATE
	// NOTE: this identifies which elements and nodes are in the wound
	//
	std::cout<<"filling dofs...\n";
	fillDOFmap(myTissue);

	std::cout<<"going to eval jacobians...\n";
	evalElemJacobians(myTissue);

	sparseLoadSolver(myTissue, filename + "_ExToInVivo_");

	//----------------------------------------------------------//

	//----------------------------------------------------------//
	// 2) WOUND INFLICTION
	// now reduce mechanical model coefficients to ~0 for wound elements
	tissue myTissue_InVivo = myTissue;
	myTissue_InVivo.ip_FF_NS = myTissue.ip_FF; //store deformation gradient from reference to in vivo

	// check which nodes belong to the wound and adjust their c and rho values
	// NOTE: check is based on deformed state after previous simulation
	std::vector<int> nodeWound;
	for(int nodei=0;nodei<n_node;nodei++){
		double x_coord = myTissue.node_x[nodei](0);
		double y_coord = myTissue.node_x[nodei](1);
		// check if nodei is inside the wound, if so impose wound values for rho and c
		double check_ellipse = pow((x_coord-x_center)*cos(alpha_ellipse)+(y_coord-y_center)*sin(alpha_ellipse),2)/(x_axis*x_axis) +\
					pow((x_coord-x_center)*sin(alpha_ellipse)+(y_coord-y_center)*cos(alpha_ellipse),2)/(y_axis*y_axis) ;
		if(check_ellipse<=1.001){
			// inside
			// add element to wound list
			nodeWound.push_back(nodei);
			// modify rho
			myTissue_InVivo.node_rho_0[nodei] = rho_wound;
			myTissue_InVivo.node_rho[nodei]   = rho_wound;
			// modify c
			myTissue_InVivo.node_c_0[nodei]   = c_wound;
			myTissue_InVivo.node_c[nodei]     = c_wound;
			// modify p
			myTissue_InVivo.node_p_0[nodei]   = prec_wound;
			myTissue_InVivo.node_p[nodei]     = prec_wound;
		}
	}
	int n_nodeWound = nodeWound.size();

	// check which elements belong to the wound and adjust structural variables
	// NOTE: check is based on deformed state after previous simulation
	std::vector<int> elemWound;
	for(int elemi=0;elemi<n_elem;elemi++){
		double xi  = 1./3.;
		double eta = 1./3.;
	 	std::vector<double> R   ={1.-xi-eta,xi,eta};
		Vector2d X_IP;X_IP.setZero();
		for(int nodej=0;nodej<3;nodej++){
			X_IP += R[nodej]*myTissue_InVivo.node_x[myTissue_InVivo.Tri[elemi][nodej]];
		}
		double check_ellipse_ip = pow((X_IP(0)-x_center)*cos(alpha_ellipse)+(X_IP(1)-y_center)*sin(alpha_ellipse),2)/(x_axis*x_axis) +\
					pow((X_IP(0)-x_center)*sin(alpha_ellipse)+(X_IP(1)-y_center)*cos(alpha_ellipse),2)/(y_axis*y_axis) ;
		if(check_ellipse_ip<=1.001){
			// inside
			// add element to wound list
			elemWound.push_back(elemi);
			// collagen fiber family
			myTissue_InVivo.ip_phic_0[elemi]   = phic0_wound;
			myTissue_InVivo.ip_phic[elemi]     = phic0_wound;
			myTissue_InVivo.ip_kappac_0[elemi] = kappac0_wound;
			myTissue_InVivo.ip_kappac[elemi]   = kappac0_wound;
			// fibronectin fiber family
			myTissue_InVivo.ip_kf_0[elemi]     = kf0_wound;
			myTissue_InVivo.ip_kf[elemi]       = kf0_wound;
			myTissue_InVivo.ip_kappaf_0[elemi] = kappaf0_wound;
			myTissue_InVivo.ip_kappaf[elemi]   = kappaf0_wound;
			// mechanical parameters
			myTissue_InVivo.ip_mu_0[elemi]     = mu0_wound;
			myTissue_InVivo.ip_mu[elemi]       = mu0_wound;
			myTissue_InVivo.ip_kc_0[elemi]     = kc0_wound;
			myTissue_InVivo.ip_kc[elemi]       = kc0_wound;
		}
	}
	int n_elemWound = elemWound.size();

	// then solve mechanical equilibrium again
	// NOTE: this also quantifies the strains in the wound core

	std::cout<<"filling dofs...\n";
	fillDOFmap(myTissue_InVivo);
	
	std::cout<<"going to eval jacobians...\n";
	evalElemJacobians(myTissue_InVivo);

	equilMechanics(myTissue_InVivo, filename + "_WoundInfliction_");

	//----------------------------------------------------------//

	//----------------------------------------------------------//
	// 3) WOUND HEALING TIME COURSE

	// first impose virgin state in wounded area
	tissue myTissue_InVivo_Wounded = myTissue_InVivo;

	double C10_hat = 0.3; //initial value for fibrin clot mechanics
	for(int elemi=0;elemi<n_elemWound;elemi++){ //loop over wound elements and set lambdaP = lambdaE inside wound

		int elemWi  = elemWound[elemi];
		// Some preliminary math...
		// get principal directions for the strains
		Matrix2d FF = myTissue_InVivo.ip_FF[elemWi];
		Matrix2d CC = FF.transpose()*FF;
		EigenSolver<Matrix2d> es(CC);
		// then eigenvalues and eigenvectors
		std::vector<Vector2d> Ubasis(2,Vector2d(0.0,0.0));
		Matrix2d CC_eiv = es.eigenvectors().real();
		Ubasis[0]       =  CC_eiv.col(0);
		Ubasis[1]       =  CC_eiv.col(1);
		Vector2d Clamda =  es.eigenvalues().real();
		Vector2d lamdaE;
		lamdaE(0)   = sqrt(Clamda(0));
		lamdaE(1)   = sqrt(Clamda(1));
		// then set permanent stretch directions
		myTissue_InVivo_Wounded.ip_a0c_0[elemWi]    = Ubasis[0];
		myTissue_InVivo_Wounded.ip_a0c[elemWi]      = Ubasis[0];
		myTissue_InVivo_Wounded.ip_a0f_0[elemWi]    = Ubasis[0];
		myTissue_InVivo_Wounded.ip_a0f[elemWi]      = Ubasis[0];
		// finally set permanent stretches
		myTissue_InVivo_Wounded.ip_lamdaP_0[elemWi] = lamdaE;
		myTissue_InVivo_Wounded.ip_lamdaP[elemWi]   = lamdaE;
		// set fibrin values
		myTissue_InVivo_Wounded.ip_phif_0[elemWi]   = phif0_wound;
		myTissue_InVivo_Wounded.ip_phif[elemWi]     = phif0_wound;
		// set value of mu0 in wound from fibrin content
		myTissue_InVivo_Wounded.ip_mu_0[elemWi]     = 2.0 * C10_hat * phif0_wound;
		myTissue_InVivo_Wounded.ip_mu[elemWi]       = 2.0 * C10_hat * phif0_wound;
	}

	// equilibrate mechanics once more
	// NOTE: this defines the 'just wounded' state
	
	std::cout<<"filling dofs...\n";
	fillDOFmap(myTissue_InVivo_Wounded);

	std::cout<<"going to eval jacobians...\n";
	evalElemJacobians(myTissue_InVivo_Wounded);

	equilMechanics(myTissue_InVivo_Wounded, filename + "_Wounded_");

	tissue myTissue_InVivo_Healing = myTissue_InVivo_Wounded; //initialize new tissue structure based on results of last equilibration
	myTissue_InVivo_Healing.ip_FF_d0wound = myTissue_InVivo_Wounded.ip_FF; //store deformation gradient right after wounding

	// then prepare files to save values from a node and an IP
	std::vector<int> save_node;save_node.clear();
	std::vector<int> save_ip;save_ip.clear();

	std::cout<<"filling dofs...\n";
	fillDOFmap(myTissue_InVivo_Healing);

	std::cout<<"going to eval jacobians...\n";
	evalElemJacobians(myTissue_InVivo_Healing);

	// finally simulate healing process
	sparseWoundSolver(myTissue_InVivo_Healing, filename + "_Healing_", freq, save_node, save_ip);

	//----------------------------------------------------------//

  return 0;
}