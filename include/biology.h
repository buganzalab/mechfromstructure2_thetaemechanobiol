/* 
	Header for constitutive equations of the wound biology
*/

#ifndef biology_h
#define biology_h

#include <vector>
#include <map>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

// Evaluate the active stress of the fibroblasts as function 
// of tissue composition and chemical signal

//------------------------------------------------------------------------------------//
// 	biology of global variables
//------------------------------------------------------------------------------------//

// for the flux function
Matrix2d evalSS_act(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP,const std::vector<double>&global_parameters, double load );

// for the tangent function
void evalSS_act(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>&global_parameters,double load,
	Matrix2d &SS_act, Matrix3d &DDact, Matrix2d &dSS_actdrho_explicit, Matrix2d &dSS_actdc_explicit);

// Eval flux and source terms for the cell and the chemical

// Flux for rho for flux
Vector2d evalQ_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters);
// Flux, plus all intermediate calculations
void evalQ_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters,
	std::vector<std::vector<double>> &Q_rho_all);
// Flux of rho and Tangent
void  evalQ_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP,  const std::vector<double> &global_parameters,
	Vector2d &Q_rho, Matrix2d &dQ_rhoxdCC_explicit, Matrix2d &dQ_rhoydCC_explicit, Vector2d &dQ_rhodrho_explicit, Matrix2d &dQ_rhodGradrho, Vector2d &dQ_rhodc_explicit, Matrix2d &dQ_rhodGradc);

// Flux of chemical
Vector2d evalQ_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP,  const std::vector<double> &global_parameters);

// flux and tangent of chemical
void  evalQ_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters,
	Vector2d &Q_rho, Matrix2d &dQ_cxdCC_explicit, Matrix2d &dQ_cydCC_explicit, Vector2d &dQ_cdrho_explicit, Matrix2d &dQ_cdGradrho, Vector2d &dQ_cdc_explicit, Matrix2d &dQ_cdGradc);


// source of rho
double evalS_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters);

// source of rho for tangent
void evalS_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, 
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters,
	double &S_rho,Matrix2d &dSrhodCC_explicit,double &dSrhodrho_explicit, double &dSrhodc_explicit);

// source of chemical
double evalS_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, double prec,
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters);

// source of the chemical for the tangent
void evalS_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, double prec,
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters,
	double &S_c,Matrix2d &dScdCC_explicit,double &dScdrho_explicit, double &dScdc_explicit);


//------------------------------------------------------------------------------------//
// 	biology of local variables
//------------------------------------------------------------------------------------//

// collagen profuction and degradation
double eval_phic_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void  eval_phic_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phic_dot_plus,  Matrix2d &dphicdotplusdCC, double &dphicdotplusdrho, double &dphicdotplusdc);
//
double eval_phic_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void eval_phic_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phic_dot_minus, Matrix2d &dphicdotminusdCC, double &dphicdotminusdrho, double& dphicdotminusdc);
	
// fibronectin production and degradation
double eval_phif_dot_plus(
	const Matrix2d &CC,double rho, double c, 
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void eval_phif_dot_plus(
	const Matrix2d &CC,double rho, double c, 
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phif_dot_plus,  Matrix2d &dphifdotplusdCC, double &dphifdotplusdrho, double &dphifdotplusdc);
//
double eval_phif_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void eval_phif_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phif_dot_minus, Matrix2d &dphifdotminusdCC, double &dphifdotminusdrho, double& dphifdotminusdc);
	
// collagen stiffness changes
double eval_kc_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void eval_kc_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kc_dot_plus,  Matrix2d &dkcdotplusdCC,  double &dkcdotplusdrho, double &dkcdotplusdc);
//
double eval_kc_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void eval_kc_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kc_dot_minus, Matrix2d &dkcdotminusdCC, double &dkcdotminusdrho, double &dkcdotminusdc);

// fibronectin stiffness changes
double eval_kf_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void eval_kf_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kf_dot_plus,  Matrix2d &dkfdotplusdCC,  double &dkfdotplusdrho, double &dkfdotplusdc);
//
double eval_kf_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters);
void  eval_kf_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double phic,double kc,const Vector2d &a0c, double kappac,
	double phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kf_dot_minus, Matrix2d &dkfdotminusdCC, double &dkfdotminusdrho, double &dkfdotminusdc);
		

#endif