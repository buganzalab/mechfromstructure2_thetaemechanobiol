/*

WOUND

This code is the implementation of the wound model
Particularly, the Dalai Lama Wound Healing, or DaLaWoHe

*/

#ifndef wound_h
#define wound_h

#include <vector>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;


//========================================================//
// RESIDUAL AND TANGENT
//
void evalWound(
	double dt,double local_dt,
	const std::vector<Matrix2d> &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,
	const std::vector<double> &ip_phic_0,const std::vector<double> &ip_kc_0,const std::vector<Vector2d> &ip_a0c_0,const std::vector<double> &ip_kappac_0, 
	const std::vector<double> &ip_phif_0,const std::vector<double> &ip_kf_0,const std::vector<Vector2d> &ip_a0f_0,const std::vector<double> &ip_kappaf_0, 
	const std::vector<Vector2d> &ip_lamdaP_0,
	const std::vector<double> &node_rho, const std::vector<double> &node_c,
	 std::vector<double> &ip_phic, std::vector<double> &ip_kc, std::vector<Vector2d> &ip_a0c, std::vector<double> &ip_kappac, 
	 std::vector<double> &ip_phif, std::vector<double> &ip_kf, std::vector<Vector2d> &ip_a0f, std::vector<double> &ip_kappaf, 
	 std::vector<Vector2d> &ip_lamdaP,
	const std::vector<Vector2d> &node_x,
	VectorXd &Re_x,MatrixXd &Ke_x_x,MatrixXd &Ke_x_rho,MatrixXd &Ke_x_c,
	VectorXd &Re_rho,MatrixXd &Ke_rho_x, MatrixXd &Ke_rho_rho,MatrixXd &Ke_rho_c,
	VectorXd &Re_c,MatrixXd &Ke_c_x,MatrixXd &Ke_c_rho,MatrixXd &Ke_c_c);
//
void evalMechanics(
	double load_step,double load,
	const std::vector<Matrix2d> &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,
	const std::vector<double> &ip_phic_0,const std::vector<double> &ip_kc_0,const std::vector<Vector2d> &ip_a0c_0,const std::vector<double> &ip_kappac_0, 
	const std::vector<double> &ip_phif_0,const std::vector<double> &ip_kf_0,const std::vector<Vector2d> &ip_a0f_0,const std::vector<double> &ip_kappaf_0, 
	const std::vector<Vector2d> &ip_lamdaP_0,
	const std::vector<Vector2d> &node_x,
	VectorXd &Re_x,MatrixXd &Ke_x_x);
//
//========================================================//



//========================================================//
// EVAL SOURCE AND FLUX 
//
void evalFluxesSources(
	const std::vector<double> &global_parameters, 
	double phic, double kc, const Vector2d &a0c,double kappac,
	double phif, double kf, const Vector2d &a0f,double kappaf,
	const Vector2d &lampdaP,
	const Matrix2d &FF,double rho, double c, const Vector2d &Grad_rho, const Vector2d &Grad_c,
	Matrix2d & SS,Vector2d &Q_rho,double &S_rho, Vector2d &Q_c,double &S_c);
//
//========================================================//



//========================================================//
// LOCAL PROBLEM: structural update
//
void localWoundProblem(
	double dt,double local_dt, 
	const std::vector<double> &local_parameters,
	double C,double rho,const Matrix2d &CC,
	double phic_0,double kc_0, const Vector2d &a0c_0, double kappac_0, 
	double phif_0,double kf_0, const Vector2d &a0f_0, double kappaf_0, 
	const Vector2d &lamdaP_0,
	double &phic,double &kc, Vector2d &a0c, double &kappac, 
	double &phif,double &kf, Vector2d &a0f, double &kappaf, 
	 Vector2d &lamdaP,
	VectorXd &dThetadCC, VectorXd &dThetadrho, VectorXd &dThetadC);
//
//========================================================//



/////////////////////////////////////////////////////////////////////////////////////////
// GEOMETRY and ELEMENT ROUTINES
/////////////////////////////////////////////////////////////////////////////////////////

//-----------------------------//
// Jacobians, at all ip and xi,eta
//
std::vector<Matrix2d> evalJacobian(const std::vector<Vector2d> node_X);
//
Matrix2d evalJacobian(const std::vector<Vector2d> node_X, double xi, double eta);
//
//-----------------------------//

//-----------------------------//
// Integration points
//
std::vector<Vector3d> LineQuadriIP();
//
//-----------------------------//

//-----------------------------//
// Basis functions
//
std::vector<double> evalShapeFunctionsR(double xi,double eta);
std::vector<double> evalShapeFunctionsRxi(double xi,double eta);
std::vector<double> evalShapeFunctionsReta(double xi,double eta);
//
//-----------------------------//



/////////////////////////////////////////////////////////////////////////////////////////
// OTHER (numerical tangent routines and such)
/////////////////////////////////////////////////////////////////////////////////////////

//========================================================//
// RESIDUAL only
//
void evalWound(
	double dt,double local_dt,
	const std::vector<Matrix2d> &ip_Jac,
	const std::vector<double> &global_parameters,const std::vector<double> &local_parameters,
	const std::vector<double> &node_rho_0, const std::vector<double> &node_c_0,
	const std::vector<double> &ip_phic_0,const std::vector<double> &ip_kc_0,const std::vector<Vector2d> &ip_a0c_0,const std::vector<double> &ip_kappac_0, 
	const std::vector<double> &ip_phif_0,const std::vector<double> &ip_kf_0,const std::vector<Vector2d> &ip_a0f_0,const std::vector<double> &ip_kappaf_0, 
	const std::vector<Vector2d> &ip_lamdaP_0,
	const std::vector<double> &node_rho, const std::vector<double> &node_c,
	 std::vector<double> &ip_phic, std::vector<double> &ip_kc, std::vector<Vector2d> &ip_a0c, std::vector<double> &ip_kappac, 
	 std::vector<double> &ip_phif, std::vector<double> &ip_kf, std::vector<Vector2d> &ip_a0f, std::vector<double> &ip_kappaf, 
	 std::vector<Vector2d> &ip_lamdaP,
	const std::vector<Vector2d> &node_x,
	VectorXd &Re_x, VectorXd &Re_rho, VectorXd &Re_c);
//
//========================================================//


#endif