/* implementation of utilities */


#include "utilities_tri.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <math.h>
#include "wound_tri.h"
#include "solver_tri.h"
#include <Eigen/Dense> // most of the vector functions I will need inside of an element


using namespace Eigen;

//-------------------------------------------------------------------------------------//
// IO
//-------------------------------------------------------------------------------------//


//---------------------------------------//
// READ COMSOL DERIVED FILE
//---------------------------------------//
//
// read in the text file derived from a COMSOL mesh file and generate the own mesh and fill in
void readComsolEditInput(const char* filename,tissue &myTissue)
{
	// READ NODES
	std::vector<Vector2d> node_X; node_X.clear();
	std::ifstream myfile(filename);
	std::string line;
	std::string keyword_node = "Nodes";
	double auxX, auxY;
	if (myfile.is_open())
	{
		// read in until you find the keyword Nodes
		while ( getline (myfile,line) )
    	{
      		// check for the keyword
      		std::size_t found = line.find(keyword_node);
			if (found!=std::string::npos)
      		{
      			getline (myfile,line);
				std::stringstream ss0(line);
				ss0>>myTissue.n_node;
      			// found the beginning of the nodes, read the nnumber of nodes
				for(int i =0;i<myTissue.n_node;i++){
					getline (myfile,line);
					std::stringstream ss1(line);
					ss1>>auxX;
					ss1>>auxY;
					node_X.push_back(Vector2d(auxX,auxY));
				}
      		}
    	}
    }
    myfile.close();
    myTissue.node_X = node_X;

	// READ ELEMENTS
	std::vector<std::vector<int> > Tri; Tri.clear();
	myfile.open(filename);
	std::string keyword_element = "Elements";
	int auxE0,auxE1,auxE2;
	if (myfile.is_open())
	{
		// read in until you find the keyword Elements
		while ( getline (myfile,line) )
    	{
      		// check for the keyword
      		std::size_t found = line.find(keyword_element);
			if (found!=std::string::npos)
      		{
      			// found the beginning of the elements, get number of elements and then loop
      			getline (myfile,line);
				std::stringstream ss3(line);
				ss3>>myTissue.n_tri;
      			for(int i=0;i<myTissue.n_tri;i++)
      			{
      				getline (myfile,line);
      				std::stringstream ss4(line);
					ss4>>auxE0;
					ss4>>auxE1;
					ss4>>auxE2;
					std::vector<int> elemi = {auxE0,auxE1,auxE2};
					Tri.push_back(elemi);
      			}
      		}
    	}
    }
    myfile.close();
    myTissue.Tri = Tri;

}

//---------------------------------------//
// READ MY OWN FILE
//---------------------------------------//
tissue readTissue(const char* filename)
{
	// initialize the structure
	tissue myTissue;

	std::ifstream myfile(filename);
	std::string line;
	if (myfile.is_open())
	{

		// time
		getline (myfile,line);
		std::stringstream ss0(line);
		ss0>>myTissue.time;

		// residual
		getline (myfile,line);

		// time final
		getline (myfile,line);
		std::stringstream ss1(line);
		ss1>>myTissue.time_final;

		// time step
		getline (myfile,line);
		std::stringstream ss2(line);
		ss2>>myTissue.time_step;

		// tol
		getline (myfile,line);
		std::stringstream ss3(line);
		ss3>>myTissue.tol;

		// max iter
		getline (myfile,line);
		std::stringstream ss4(line);
		ss4>>myTissue.max_iter;

		// n_dof_xx
		getline (myfile,line);
		std::stringstream ss999(line);
		ss999>>myTissue.n_dof_xx;

		// local time step
		getline (myfile,line);
		std::stringstream ss998(line);
		ss998>>myTissue.local_time_step;

		// max time step doubling
		getline (myfile,line);
		std::stringstream ss997(line);
		ss997>>myTissue.max_time_step_doublings;

		// load step
		getline (myfile,line);
		std::stringstream ss996(line);
		ss996>>myTissue.load_step;

		// load
		getline (myfile,line);
		std::stringstream ss995(line);
		ss995>>myTissue.load;

		// max load step doublings
		getline (myfile,line);
		std::stringstream ss994(line);
		ss994>>myTissue.max_load_step_doublings;

		// global parameters
		int n_global_parameters;
		getline (myfile,line);
		std::stringstream ss5(line);
		ss5>>n_global_parameters;
		std::vector<double> global_parameters(n_global_parameters,0.);
		getline (myfile,line);
		std::stringstream ss6(line);
		for(int i=0;i<n_global_parameters;i++){
			ss6>>global_parameters[i];
		}
		myTissue.global_parameters = global_parameters;

		// local parameters
		int n_local_parameters;
		getline (myfile,line);
		std::stringstream ss7(line);
		ss7>>n_local_parameters;
		std::vector<double> local_parameters(n_local_parameters,0.);
		getline (myfile,line);
		std::stringstream ss8(line);
		for(int i=0;i<n_local_parameters;i++){
			ss8>>local_parameters[i];
		}
		myTissue.local_parameters = local_parameters;

		// n_node
		getline (myfile,line);
		std::stringstream ss9(line);
		ss9>>myTissue.n_node;

		// n_tri
		getline (myfile,line);
		std::stringstream ss10(line);
		ss10>>myTissue.n_tri;

		// n_IP
		getline (myfile,line);
		std::stringstream ss11(line);
		ss11>>myTissue.n_IP;
		if(myTissue.n_IP>1*myTissue.n_tri || myTissue.n_IP<1*myTissue.n_tri )
		{std::cout<<"number of integration points and elements don't match\n";myTissue.n_IP = 1*myTissue.n_tri;}

		// n_dof
		getline (myfile,line);
		std::stringstream ss12(line);
		ss12>>myTissue.n_dof;

		// LineTri
		std::vector<int> temp_elem(3,0);
		std::vector<std::vector<int > > Tri(myTissue.n_tri,temp_elem);
		myTissue.Tri = Tri;
		for(int i=0;i<myTissue.Tri.size();i++){
			getline (myfile,line);
			std::stringstream ss13(line);
			ss13>>myTissue.Tri[i][0]; ss13>>myTissue.Tri[i][1]; ss13>>myTissue.Tri[i][2];
		}

		// node_X
		std::vector<Vector2d> node_X(myTissue.n_node,Vector2d(0,0));
		myTissue.node_X = node_X;
		for(int i=0;i<myTissue.node_X.size();i++){
			getline (myfile,line);
			std::stringstream ss15(line);
			ss15>>myTissue.node_X[i](0);ss15>>myTissue.node_X[i](1);
		}

		// node_rho_0
		std::vector<double> node_rho_0(myTissue.n_node,0.0);
		myTissue.node_rho_0 = node_rho_0;
		for(int i=0;i<myTissue.node_rho_0.size();i++){
			getline (myfile,line);
			std::stringstream ss16(line);
			ss16>>myTissue.node_rho_0[i];
		}

		// node_c_0
		std::vector<double> node_c_0(myTissue.n_node,0.0);
		myTissue.node_c_0 = node_c_0;
		for(int i=0;i<myTissue.node_c_0.size();i++){
			getline (myfile,line);
			std::stringstream ss17(line);
			ss17>>myTissue.node_c_0[i];
		}

		// ip_phic_0
		std::vector<double> ip_phic_0(myTissue.n_IP,0.0);
		myTissue.ip_phic_0 = ip_phic_0;
		for(int i=0;i<myTissue.ip_phic_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_phic_0[i];
		}

		// ip_mu_0
		std::vector<double> ip_mu_0(myTissue.n_IP,0.0);
		myTissue.ip_mu_0 = ip_mu_0;
		for(int i=0;i<myTissue.ip_mu_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_mu_0[i];
		}

		// ip_kc_0
		std::vector<double> ip_kc_0(myTissue.n_IP,0.0);
		myTissue.ip_kc_0 = ip_kc_0;
		for(int i=0;i<myTissue.ip_kc_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_kc_0[i];
		}

		// ip_a0c_0
		std::vector<Vector2d> ip_a0c_0(myTissue.n_IP,Vector2d(0,0));
		myTissue.ip_a0c_0 = ip_a0c_0;
		for(int i=0;i<myTissue.ip_a0c_0.size();i++){
			getline (myfile,line);
			std::stringstream ss19(line);
			ss19>>myTissue.ip_a0c_0[i](0);ss19>>myTissue.ip_a0c_0[i](1);
		}

		// ip_kappac_0
		std::vector<double> ip_kappac_0(myTissue.n_IP,0.0);
		myTissue.ip_kappac_0 = ip_kappac_0;
		for(int i=0;i<myTissue.ip_kappac_0.size();i++){
			getline (myfile,line);
			std::stringstream ss20(line);
			ss20>>myTissue.ip_kappac_0[i];
		}

		// ip_phif_0
		std::vector<double> ip_phif_0(myTissue.n_IP,0.0);
		myTissue.ip_phif_0 = ip_phif_0;
		for(int i=0;i<myTissue.ip_phif_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_phif_0[i];
		}

		// ip_kf_0
		std::vector<double> ip_kf_0(myTissue.n_IP,0.0);
		myTissue.ip_kf_0 = ip_kf_0;
		for(int i=0;i<myTissue.ip_kf_0.size();i++){
			getline (myfile,line);
			std::stringstream ss18(line);
			ss18>>myTissue.ip_kf_0[i];
		}

		// ip_a0f_0
		std::vector<Vector2d> ip_a0f_0(myTissue.n_IP,Vector2d(0,0));
		myTissue.ip_a0f_0 = ip_a0f_0;
		for(int i=0;i<myTissue.ip_a0f_0.size();i++){
			getline (myfile,line);
			std::stringstream ss19(line);
			ss19>>myTissue.ip_a0f_0[i](0);ss19>>myTissue.ip_a0f_0[i](1);
		}

		// ip_kappaf_0
		std::vector<double> ip_kappaf_0(myTissue.n_IP,0.0);
		myTissue.ip_kappaf_0 = ip_kappaf_0;
		for(int i=0;i<myTissue.ip_kappaf_0.size();i++){
			getline (myfile,line);
			std::stringstream ss20(line);
			ss20>>myTissue.ip_kappaf_0[i];
		}

		// ip_lamdaP_0
		std::vector<Vector2d> ip_lamdaP_0(myTissue.n_IP,Vector2d(0,0));
		myTissue.ip_lamdaP_0 = ip_lamdaP_0;
		for(int i=0;i<myTissue.ip_lamdaP_0.size();i++){
			getline (myfile,line);
			std::stringstream ss21(line);
			ss21>>myTissue.ip_lamdaP_0[i](0);ss21>>myTissue.ip_lamdaP_0[i](1);
		}

		// node_x
		std::vector<Vector2d> node_x(myTissue.n_node,Vector2d(0,0));
		myTissue.node_x = node_x;
		for(int i=0;i<myTissue.node_x.size();i++){
			getline (myfile,line);
			std::stringstream ss22(line);
			ss22>>myTissue.node_x[i](0);ss22>>myTissue.node_x[i](1);
		}

		// node_rho
		myTissue.node_rho = myTissue.node_rho_0;

		// node_c
		myTissue.node_c = myTissue.node_c_0;

		// ip_phic
		myTissue.ip_phic = myTissue.ip_phic_0;

		// ip_mu
		myTissue.ip_mu = myTissue.ip_mu_0;

		// ip_kc
		myTissue.ip_kc = myTissue.ip_kc_0;

		// ip_a0c
		myTissue.ip_a0c = myTissue.ip_a0c_0;

		// ip_kappac
		myTissue.ip_kappac = myTissue.ip_kappac_0;

		// ip_phif
		myTissue.ip_phif = myTissue.ip_phif_0;

		// ip_kf
		myTissue.ip_kf = myTissue.ip_kf_0;

		// ip_a0f
		myTissue.ip_a0f = myTissue.ip_a0f_0;

		// ip_kappaf
		myTissue.ip_kappaf = myTissue.ip_kappaf_0;

		// ip_lamdaP
		myTissue.ip_lamdaP = myTissue.ip_lamdaP_0;

		// eBC_x
		int n_eBC_x;
		int dofx;
		double dofx_value;
		getline (myfile,line);
		std::stringstream ss23(line);
		ss23>>n_eBC_x;
		myTissue.eBC_x.clear();
		for(int i=0;i<n_eBC_x;i++){
			getline (myfile,line);
			std::stringstream ss24(line);
			ss24>>dofx;ss24>>dofx_value;
			myTissue.eBC_x.insert ( std::pair<int,double>(dofx,dofx_value) );
		}

		// eBC_rho
		int n_eBC_rho;
		int dofrho;
		double dofrho_value;
		getline (myfile,line);
		std::stringstream ss25(line);
		ss25>>n_eBC_rho;
		myTissue.eBC_rho.clear();
		for(int i=0;i<n_eBC_rho;i++){
			getline (myfile,line);
			std::stringstream ss26(line);
			ss26>>dofrho;ss26>>dofrho_value;
			myTissue.eBC_rho.insert ( std::pair<int,double>(dofrho,dofrho_value) );
		}

		// eBC_c
		int n_eBC_c;
		int dofc;
		double dofc_value;
		getline (myfile,line);
		std::stringstream ss27(line);
		ss27>>n_eBC_c;
		myTissue.eBC_c.clear();
		for(int i=0;i<n_eBC_c;i++){
			getline (myfile,line);
			std::stringstream ss28(line);
			ss28>>dofc;ss28>>dofc_value;
			myTissue.eBC_c.insert ( std::pair<int,double>(dofc,dofc_value) );
		}

		// nBC_x
		int n_nBC_x;
		double forcex_value;
		getline (myfile,line);
		std::stringstream ss29(line);
		ss29>>n_nBC_x;
		myTissue.nBC_x.clear();
		for(int i=0;i<n_nBC_x;i++){
			getline (myfile,line);
			std::stringstream ss30(line);
			ss30>>dofx;ss30>>forcex_value;
			myTissue.nBC_x.insert ( std::pair<int,double>(dofx,forcex_value) );
		}

		// nBC_rho
		int n_nBC_rho;
		double forcerho_value;
		getline (myfile,line);
		std::stringstream ss31(line);
		ss31>>n_nBC_rho;
		myTissue.nBC_rho.clear();
		for(int i=0;i<n_nBC_rho;i++){
			getline (myfile,line);
			std::stringstream ss32(line);
			ss32>>dofrho;ss32>>forcerho_value;
			myTissue.nBC_rho.insert ( std::pair<int,double>(dofrho,forcerho_value) );
		}

		// nBC_c
		int n_nBC_c;
		double forcec_value;
		getline (myfile,line);
		std::stringstream ss33(line);
		ss33>>n_nBC_c;
		myTissue.nBC_c.clear();
		for(int i=0;i<n_nBC_c;i++){
			getline (myfile,line);
			std::stringstream ss34(line);
			ss34>>dofc;ss34>>forcec_value;
			myTissue.nBC_c.insert ( std::pair<int,double>(dofc,forcec_value) );
		}

		// dof_fwd_map_x
		std::vector<int> dof_fwd_map_x(myTissue.n_node*2,-1);
		myTissue.dof_fwd_map_x = dof_fwd_map_x;
		for(int i=0;i<myTissue.n_node;i++){
			getline (myfile,line);
			std::stringstream ss36(line);
			ss36>>myTissue.dof_fwd_map_x[i*2+0];ss36>>myTissue.dof_fwd_map_x[i*2+1];
		}

		// dof_fwd_map_rho
		std::vector<int> dof_fwd_map_rho(myTissue.n_node,-1);
		myTissue.dof_fwd_map_rho = dof_fwd_map_rho;
		for(int i=0;i<myTissue.n_node;i++){
			getline (myfile,line);
			std::stringstream ss38(line);
			ss38>>myTissue.dof_fwd_map_rho[i];
		}

		// dof_fwd_map_c
		std::vector<int> dof_fwd_map_c(myTissue.n_node,-1);
		myTissue.dof_fwd_map_c = dof_fwd_map_c;
		for(int i=0;i<myTissue.n_node;i++){
			getline (myfile,line);
			std::stringstream ss40(line);
			ss40>>myTissue.dof_fwd_map_c[i];
		}

		// dof_inv_map
		int n_dof_inv_map;
		getline (myfile,line);
		std::stringstream ss41(line);
		ss41>>n_dof_inv_map;
		std::vector<int> temp_inv_dof(2,0);
		std::vector<std::vector<int> > dof_inv_map(n_dof_inv_map,temp_inv_dof);
		myTissue.dof_inv_map = dof_inv_map;
		for(int i=0;i<myTissue.dof_inv_map.size();i++){
			getline (myfile,line);
			std::stringstream ss42(line);
			ss42>>myTissue.dof_inv_map[i][0];ss42>>myTissue.dof_inv_map[i][1];
		}
	}
	myfile.close();
	evalElemJacobians(myTissue);
	return myTissue;
}


//---------------------------------------//
// WRITE OUT MY OWN FILE
//---------------------------------------//
//
void writeTissue(tissue &myTissue, const char* filename,double time, double res)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<time<<"\n";
	savefile<<res<<"\n";
	savefile<<myTissue.time_final<<"\n";
	savefile<<myTissue.time_step<<"\n";
	savefile<<myTissue.tol<<"\n";
	savefile<<myTissue.max_iter<<"\n";
	savefile<<myTissue.n_dof_xx<<"\n";
	savefile<<myTissue.local_time_step<<"\n";
	savefile<<myTissue.max_time_step_doublings<<"\n";
	savefile<<myTissue.load_step<<"\n";
	savefile<<myTissue.load<<"\n";
	savefile<<myTissue.max_load_step_doublings<<"\n";

	savefile<<myTissue.global_parameters.size()<<"\n";
	for(int i=0;i<myTissue.global_parameters.size();i++){
		savefile<<myTissue.global_parameters[i]<<" ";
	}
	savefile<<"\n";
	savefile<<myTissue.local_parameters.size()<<"\n";
	for(int i=0;i<myTissue.local_parameters.size();i++){
		savefile<<myTissue.local_parameters[i]<<" ";
	}
	savefile<<"\n";
	savefile<<myTissue.n_node<<"\n";
	savefile<<myTissue.n_tri<<"\n";
	savefile<<myTissue.n_IP<<"\n";
	savefile<<myTissue.n_dof<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++){
		savefile<<myTissue.Tri[i][0]<<" "<<myTissue.Tri[i][1]<<" "<<myTissue.Tri[i][2]<<"\n";
	}
	for(int i=0;i<myTissue.node_X.size();i++){
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.node_rho_0.size();i++){
		savefile<<myTissue.node_rho_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.node_c_0.size();i++){
		savefile<<myTissue.node_c_0[i]<<"\n";
	}

	for(int i=0;i<myTissue.ip_phic_0.size();i++){
		savefile<<myTissue.ip_phic_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_mu_0.size();i++){
		savefile<<myTissue.ip_mu_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_kc_0.size();i++){
		savefile<<myTissue.ip_kc_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_a0c_0.size();i++){
		savefile<<myTissue.ip_a0c_0[i](0)<<" "<<myTissue.ip_a0c_0[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.ip_kappac_0.size();i++){
		savefile<<myTissue.ip_kappac_0[i]<<"\n";
	}

	for(int i=0;i<myTissue.ip_phif_0.size();i++){
		savefile<<myTissue.ip_phif_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_kf_0.size();i++){
		savefile<<myTissue.ip_kf_0[i]<<"\n";
	}
	for(int i=0;i<myTissue.ip_a0f_0.size();i++){
		savefile<<myTissue.ip_a0f_0[i](0)<<" "<<myTissue.ip_a0f_0[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.ip_kappaf_0.size();i++){
		savefile<<myTissue.ip_kappaf_0[i]<<"\n";
	}

	for(int i=0;i<myTissue.ip_lamdaP_0.size();i++){
		savefile<<myTissue.ip_lamdaP_0[i](0)<<" "<<myTissue.ip_lamdaP_0[i](1)<<"\n";
	}
	for(int i=0;i<myTissue.node_x.size();i++){
		savefile<<myTissue.node_x[i](0)<<" "<<myTissue.node_x[i](1)<<"\n";
	}
	std::map<int,double>::iterator it_map_BC;
	savefile<<myTissue.eBC_x.size()<<"\n";
	for(it_map_BC = myTissue.eBC_x.begin(); it_map_BC != myTissue.eBC_x.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.eBC_rho.size()<<"\n";
	for(it_map_BC = myTissue.eBC_rho.begin(); it_map_BC != myTissue.eBC_rho.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.eBC_c.size()<<"\n";
	for(it_map_BC = myTissue.eBC_c.begin(); it_map_BC != myTissue.eBC_c.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.nBC_x.size()<<"\n";
	for(it_map_BC = myTissue.nBC_x.begin(); it_map_BC != myTissue.nBC_x.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.nBC_rho.size()<<"\n";
	for(it_map_BC = myTissue.nBC_rho.begin(); it_map_BC != myTissue.nBC_rho.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}
	savefile<<myTissue.nBC_c.size()<<"\n";
	for(it_map_BC = myTissue.nBC_c.begin(); it_map_BC != myTissue.nBC_c.end(); it_map_BC++) {
    	// iterator->first = key
    	// iterator->second = value
		savefile<<it_map_BC->first<<" "<<it_map_BC->second<<"\n";
	}

	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.dof_fwd_map_x[i*2+0]<<" "<<myTissue.dof_fwd_map_x[i*2+1]<<"\n";
	}

	for(int i=0;i<myTissue.dof_fwd_map_rho.size();i++){
		savefile<<myTissue.dof_fwd_map_rho[i]<<"\n";
	}

	for(int i=0;i<myTissue.dof_fwd_map_c.size();i++){
		savefile<<myTissue.dof_fwd_map_c[i]<<"\n";
	}

	savefile<<myTissue.dof_inv_map.size()<<"\n";
	for(int i=0;i<myTissue.dof_inv_map.size();i++){
		savefile<<myTissue.dof_inv_map[i][0]<<" "<<myTissue.dof_inv_map[i][1]<<"\n";
	}
	savefile.close();
}

//---------------------------------------//
// WRITE OUT A PARAVIEW FILE
//---------------------------------------//
void writeParaviewBiol(tissue &myTissue, const char* filename, double res)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<"# vtk DataFile Version 2.0\nWound Time: "<<myTissue.time<<", Residual: "<<res<<"\nASCII\nDATASET UNSTRUCTURED_GRID\n";
	savefile<<"POINTS "<<myTissue.node_x.size()<<" double\n";
	for(int i=0;i<myTissue.node_x.size();i++)
	{
		//reference configuration
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<" 0.0\n";
		//deformed configuration
		// savefile<<myTissue.node_x[i](0)<<" "<<myTissue.node_x[i](1)<<" 0.0\n";
	}
	savefile<<"CELLS "<<myTissue.Tri.size()<<" "<<myTissue.Tri.size()*4<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++)
	{
		savefile<<"3";
		for(int j=0;j<3;j++)
		{
			savefile<<" "<<myTissue.Tri[i][j];
		}
		savefile<<"\n";
	}
	savefile<<"CELL_TYPES "<<myTissue.Tri.size()<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++)
	{
		savefile<<"5\n";
	}
	//
	// SAVE ATTRIBUTES
	// up to four scalars I can plot...
	// first bring back from the integration points to the nodes
	std::vector<double> node_phic(myTissue.n_node,0);
	std::vector<double> node_mu(myTissue.n_node,0);
	std::vector<double> node_kc(myTissue.n_node,0);
	std::vector<Vector2d> node_a0c(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappac(myTissue.n_node,0);
	//
	std::vector<double> node_phif(myTissue.n_node,0);
	std::vector<double> node_kf(myTissue.n_node,0);
	std::vector<Vector2d> node_a0f(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappaf(myTissue.n_node,0);
	//
	std::vector<Vector2d> node_lamdaP(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_thetaP(myTissue.n_node,0);
	std::vector<double> node_thetaE(myTissue.n_node,0);
	//
	std::vector<int> node_ip_count(myTissue.n_node,0);
	for(int elemi=0;elemi<myTissue.n_tri;elemi++){
		for(int ni=0;ni<3;ni++){
			node_phic[myTissue.Tri[elemi][ni]]+=myTissue.ip_phic[elemi];
			node_mu[myTissue.Tri[elemi][ni]]+=myTissue.ip_mu[elemi];
			node_kc[myTissue.Tri[elemi][ni]]+=myTissue.ip_kc[elemi];
			node_a0c[myTissue.Tri[elemi][ni]]+=myTissue.ip_a0c[elemi];
			node_kappac[myTissue.Tri[elemi][ni]]+=myTissue.ip_kappac[elemi];
			//
			node_phif[myTissue.Tri[elemi][ni]]+=myTissue.ip_phif[elemi];
			node_kf[myTissue.Tri[elemi][ni]]+=myTissue.ip_kf[elemi];
			node_a0f[myTissue.Tri[elemi][ni]]+=myTissue.ip_a0f[elemi];
			node_kappaf[myTissue.Tri[elemi][ni]]+=myTissue.ip_kappaf[elemi];
			//
			node_lamdaP[myTissue.Tri[elemi][ni]]+=myTissue.ip_lamdaP[elemi];
			node_thetaP[myTissue.Tri[elemi][ni]]+=myTissue.ip_lamdaP[elemi](0)*myTissue.ip_lamdaP[elemi](1);
			node_thetaE[myTissue.Tri[elemi][ni]]+=myTissue.ip_FF[elemi].determinant()/myTissue.ip_lamdaP[elemi](0)/myTissue.ip_lamdaP[elemi](1);
			//
			node_ip_count[myTissue.Tri[elemi][ni]] += 1;
		}
	}
	for(int nodei=0;nodei<myTissue.n_node;nodei++){
		node_phic[nodei] = node_phic[nodei]/node_ip_count[nodei];
		node_mu[nodei] = node_mu[nodei]/node_ip_count[nodei];
		node_kc[nodei] = node_kc[nodei]/node_ip_count[nodei];
		node_a0c[nodei] = node_a0c[nodei]/node_ip_count[nodei];
		node_kappac[nodei] = node_kappac[nodei]/node_ip_count[nodei];
		//
		node_phif[nodei] = node_phif[nodei]/node_ip_count[nodei];
		node_kf[nodei] = node_kf[nodei]/node_ip_count[nodei];
		node_a0f[nodei] = node_a0f[nodei]/node_ip_count[nodei];
		node_kappaf[nodei] = node_kappaf[nodei]/node_ip_count[nodei];
		//
		node_lamdaP[nodei] = node_lamdaP[nodei]/node_ip_count[nodei];
		node_thetaP[nodei] = node_thetaP[nodei]/node_ip_count[nodei];
		node_thetaE[nodei] = node_thetaE[nodei]/node_ip_count[nodei];
	}
	// c, rho, thetaP, thetaE
	savefile<<"POINT_DATA "<<myTissue.n_node<<"\nSCALARS c_rho_thetaP_thetaE float "<<4<<"\nLOOKUP_TABLE default\n";
	for(int i=0;i<myTissue.n_node;i++){
		// write out chemicals value ensuring that it is in paraview displayable range 
		if ( myTissue.node_c[i] > 0 && myTissue.node_c[i] < 2e-38)
			savefile<<"0.0 ";
		else
			savefile<<myTissue.node_c[i]<<" ";
		// write out cells value ensuring that it is in paraview displayable range 
		if ( myTissue.node_rho[i] > 0 && myTissue.node_rho[i] < 2e-38)
			savefile<<"0.0 ";
		else
			savefile<<myTissue.node_rho[i]<<" ";
		// write out plastic stretch value ensuring that it is in paraview displayable range 
		if ( node_thetaP[i] > 0 && node_thetaP[i] < 2e-38)
			savefile<<"0.0 ";
		else
			savefile<<node_thetaP[i]<<" ";
		// write out elastic stretch value ensuring that it is in paraview displayable range 
		if ( node_thetaE[i] > 0 && node_thetaE[i] < 2e-38)
			savefile<<"0.0\n";
		else
			savefile<<node_thetaE[i]<<"\n";
	}
	//
	// write out the fiber direction
	savefile<<"VECTORS u float\n";
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.node_x[i](0)-myTissue.node_X[i](0)<<" "<<myTissue.node_x[i](1)-myTissue.node_X[i](1)<<" 0\n";
	}
	//
	savefile<<"CELL_DATA "<<myTissue.Tri.size()<<"\nSCALARS lamdaPa_lamdaPs float "<<2<<"\nLOOKUP_TABLE default\n";
	// loop over triangles
	for(int ei=0;ei<myTissue.Tri.size();ei++){
		// single IP data: account for possible growth
		Vector2d a0c = myTissue.ip_a0c_0[ei];
		double lamdaP_a = myTissue.ip_lamdaP[ei](0);
		double lamdaP_s = myTissue.ip_lamdaP[ei](1);
		Matrix2d FF = myTissue.ip_FF[ei];
		Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
		Vector2d s0c = Rot90*a0c;
		Matrix2d a0ca0c = a0c*a0c.transpose();
		Matrix2d s0cs0c = s0c*s0c.transpose();
		//
		// recompute split and get elastic strain
		Matrix2d FFg = lamdaP_a*(a0ca0c) + lamdaP_s*(s0cs0c);
		double thetaP = lamdaP_a*lamdaP_s;
		Matrix2d FFginv = (1./lamdaP_a)*(a0ca0c) + (1./lamdaP_s)*(s0cs0c);
		Matrix2d FFe = FF*FFginv;
		Matrix2d Identity = Matrix2d::Identity(2,2);
		Matrix2d EEe = 0.5*(FFe.transpose()*FFe - Identity);
		savefile<<myTissue.ip_lamdaP[ei](0)<<" "<<myTissue.ip_lamdaP[ei](1)<<"\n";
	}
	
	savefile.close();
}

void writeParaviewMech(tissue &myTissue, const char* filename, double res)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<"# vtk DataFile Version 2.0\nWound Time: "<<myTissue.time<<", Residual: "<<res<<"\nASCII\nDATASET UNSTRUCTURED_GRID\n";
	savefile<<"POINTS "<<myTissue.node_x.size()<<" double\n";
	for(int i=0;i<myTissue.node_x.size();i++)
	{
		//reference configuration
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<" 0.0\n";
		//deformed configuration
		// savefile<<myTissue.node_x[i](0)<<" "<<myTissue.node_x[i](1)<<" 0.0\n";
	}
	savefile<<"CELLS "<<myTissue.Tri.size()<<" "<<myTissue.Tri.size()*4<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++)
	{
		savefile<<"3";
		for(int j=0;j<3;j++)
		{
			savefile<<" "<<myTissue.Tri[i][j];
		}
		savefile<<"\n";
	}
	savefile<<"CELL_TYPES "<<myTissue.Tri.size()<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++)
	{
		savefile<<"5\n";
	}
	//
	// SAVE ATTRIBUTES
	// up to four scalars I can plot...
	// first bring back from the integration points to the nodes
	std::vector<double> node_phic(myTissue.n_node,0);
	std::vector<double> node_mu(myTissue.n_node,0);
	std::vector<double> node_kc(myTissue.n_node,0);
	std::vector<Vector2d> node_a0c(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappac(myTissue.n_node,0);
	//
	std::vector<double> node_phif(myTissue.n_node,0);
	std::vector<double> node_kf(myTissue.n_node,0);
	std::vector<Vector2d> node_a0f(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappaf(myTissue.n_node,0);
	//
	std::vector<Vector2d> node_lamdaP(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_thetaP(myTissue.n_node,0);
	std::vector<double> node_thetaE(myTissue.n_node,0);
	//
	std::vector<int> node_ip_count(myTissue.n_node,0);
	for(int elemi=0;elemi<myTissue.n_tri;elemi++){
		for(int ni=0;ni<3;ni++){
			node_phic[myTissue.Tri[elemi][ni]]+=myTissue.ip_phic[elemi];
			node_mu[myTissue.Tri[elemi][ni]]+=myTissue.ip_mu[elemi];
			node_kc[myTissue.Tri[elemi][ni]]+=myTissue.ip_kc[elemi];
			node_a0c[myTissue.Tri[elemi][ni]]+=myTissue.ip_a0c[elemi];
			node_kappac[myTissue.Tri[elemi][ni]]+=myTissue.ip_kappac[elemi];
			//
			node_phif[myTissue.Tri[elemi][ni]]+=myTissue.ip_phif[elemi];
			node_kf[myTissue.Tri[elemi][ni]]+=myTissue.ip_kf[elemi];
			node_a0f[myTissue.Tri[elemi][ni]]+=myTissue.ip_a0f[elemi];
			node_kappaf[myTissue.Tri[elemi][ni]]+=myTissue.ip_kappaf[elemi];
			//
			node_lamdaP[myTissue.Tri[elemi][ni]]+=myTissue.ip_lamdaP[elemi];
			node_thetaP[myTissue.Tri[elemi][ni]]+=myTissue.ip_lamdaP[elemi](0)*myTissue.ip_lamdaP[elemi](1);
			node_thetaE[myTissue.Tri[elemi][ni]]+=myTissue.ip_FF[elemi].determinant()/myTissue.ip_lamdaP[elemi](0)/myTissue.ip_lamdaP[elemi](1);
			//
			node_ip_count[myTissue.Tri[elemi][ni]] += 1;
		}
	}
	for(int nodei=0;nodei<myTissue.n_node;nodei++){
		node_phic[nodei] = node_phic[nodei]/node_ip_count[nodei];
		node_mu[nodei] = node_mu[nodei]/node_ip_count[nodei];
		node_kc[nodei] = node_kc[nodei]/node_ip_count[nodei];
		node_a0c[nodei] = node_a0c[nodei]/node_ip_count[nodei];
		node_kappac[nodei] = node_kappac[nodei]/node_ip_count[nodei];
		//
		node_phif[nodei] = node_phif[nodei]/node_ip_count[nodei];
		node_kf[nodei] = node_kf[nodei]/node_ip_count[nodei];
		node_a0f[nodei] = node_a0f[nodei]/node_ip_count[nodei];
		node_kappaf[nodei] = node_kappaf[nodei]/node_ip_count[nodei];
		//
		node_lamdaP[nodei] = node_lamdaP[nodei]/node_ip_count[nodei];
		node_thetaP[nodei] = node_thetaP[nodei]/node_ip_count[nodei];
		node_thetaE[nodei] = node_thetaE[nodei]/node_ip_count[nodei];
	}
	// phic, csic=kf, C10, k1
	savefile<<"POINT_DATA "<<myTissue.n_node<<"\nSCALARS phic_csic_C10_k1 float "<<4<<"\nLOOKUP_TABLE default\n";
	for(int i=0;i<myTissue.n_node;i++){
		// write out collagen value ensuring that it is in paraview displayable range 
		if ( node_phic[i] > 0 && node_phic[i] < 2e-38)
			savefile<<"0.0 ";
		else
			savefile<<node_phic[i]<<" ";
		// write out crosslink value ensuring that it is in paraview displayable range 
		if ( node_kf[i] > 0 && node_kf[i] < 2e-38)
			savefile<<"0.0 ";
		else
			savefile<<node_kf[i]<<" ";
		// write out C10 value ensuring that it is in paraview displayable range 
		if ( 0.5*node_mu[i] > 0 && 0.5*node_mu[i] < 2e-38)
			savefile<<"0.0 ";
		else
			savefile<<0.5*node_mu[i]<<" ";
		// write out k1 value ensuring that it is in paraview displayable range 
		if ( node_kc[i] > 0 && node_kc[i] < 2e-38)
			savefile<<"0.0\n";
		else
			savefile<<node_kc[i]<<"\n";
	}
	//
	// write out the fiber direction
	savefile<<"VECTORS u float\n";
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.node_x[i](0)-myTissue.node_X[i](0)<<" "<<myTissue.node_x[i](1)-myTissue.node_X[i](1)<<" 0\n";
	}
	//
	savefile<<"CELL_DATA "<<myTissue.Tri.size()<<"\nSCALARS LagrStrainX_LagrStrainY_Fxx_Fyy float "<<4<<"\nLOOKUP_TABLE default\n";
	// loop over triangles
	for(int ei=0;ei<myTissue.Tri.size();ei++){
		// single IP data: account for possible growth
		Vector2d a0c = myTissue.ip_a0c_0[ei];
		double lamdaP_a = myTissue.ip_lamdaP[ei](0);
		double lamdaP_s = myTissue.ip_lamdaP[ei](1);
		Matrix2d FF = myTissue.ip_FF[ei];
		Matrix2d Rot90;Rot90<<0.,-1.,1.,0.;
		Vector2d s0c = Rot90*a0c;
		Matrix2d a0ca0c = a0c*a0c.transpose();
		Matrix2d s0cs0c = s0c*s0c.transpose();
		//
		// recompute split and get elastic strain
		Matrix2d FFg = lamdaP_a*(a0ca0c) + lamdaP_s*(s0cs0c);
		double thetaP = lamdaP_a*lamdaP_s;
		Matrix2d FFginv = (1./lamdaP_a)*(a0ca0c) + (1./lamdaP_s)*(s0cs0c);
		Matrix2d FFe = FF*FFginv;
		Matrix2d Identity = Matrix2d::Identity(2,2);
		Matrix2d EEe = 0.5*(FFe.transpose()*FFe - Identity);
		savefile<<EEe(0,0)<<" "<<EEe(1,1)<<" "<<myTissue.ip_FF[ei](0,0)<<" "<<myTissue.ip_FF[ei](1,1)<<"\n";
	}
	
	savefile.close();
}

void writeParaview(int FLAG, tissue &myTissue, const char* filename)
{
	std::ofstream savefile(filename);
	if (!savefile) {
		throw std::runtime_error("Unable to open output file.");
	}
	savefile<<"# vtk DataFile Version 2.0\nWound Time: "<<myTissue.time<<", FLAG: "<<FLAG<<"\nASCII\nDATASET UNSTRUCTURED_GRID\n";
	savefile<<"POINTS "<<myTissue.node_X.size()<<" double\n";
	for(int i=0;i<myTissue.node_X.size();i++)
	{
		savefile<<myTissue.node_X[i](0)<<" "<<myTissue.node_X[i](1)<<" 0.0\n";
	}
	savefile<<"CELLS "<<myTissue.Tri.size()<<" "<<myTissue.Tri.size()*4<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++)
	{
		savefile<<"3";
		for(int j=0;j<3;j++)
		{
			savefile<<" "<<myTissue.Tri[i][j];
		}
		savefile<<"\n";
	}
	savefile<<"CELL_TYPES "<<myTissue.Tri.size()<<"\n";
	for(int i=0;i<myTissue.Tri.size();i++)
	{
		savefile<<"5\n";
	}
	// SAVE ATTRIBUTES
	// up to four scalars I can plot...
	// first bring back from the integration points to the nodes
	std::vector<double> node_phic(myTissue.n_node,0);
	std::vector<double> node_mu(myTissue.n_node,0);
	std::vector<double> node_kc(myTissue.n_node,0);
	std::vector<Vector2d> node_a0c(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappac(myTissue.n_node,0);
	//
	std::vector<double> node_phif(myTissue.n_node,0);
	std::vector<double> node_kf(myTissue.n_node,0);
	std::vector<Vector2d> node_a0f(myTissue.n_node,Vector2d(0,0));
	std::vector<double> node_kappaf(myTissue.n_node,0);
	//
	std::vector<Vector2d> node_lamdaP(myTissue.n_node,Vector2d(0,0));
	std::vector<int> node_ip_count(myTissue.n_node,0);
	for(int elemi=0;elemi<myTissue.n_tri;elemi++){
		for(int ni=0;ni<3;ni++){
			node_phic[myTissue.Tri[elemi][ni]]+=myTissue.ip_phic[elemi];
			node_mu[myTissue.Tri[elemi][ni]]+=myTissue.ip_mu[elemi];
			node_kc[myTissue.Tri[elemi][ni]]+=myTissue.ip_kc[elemi];
			node_a0c[myTissue.Tri[elemi][ni]]+=myTissue.ip_a0c[elemi];
			node_kappac[myTissue.Tri[elemi][ni]]+=myTissue.ip_kappac[elemi];
			//
			node_phif[myTissue.Tri[elemi][ni]]+=myTissue.ip_phif[elemi];
			node_kf[myTissue.Tri[elemi][ni]]+=myTissue.ip_kf[elemi];
			node_a0f[myTissue.Tri[elemi][ni]]+=myTissue.ip_a0f[elemi];
			node_kappaf[myTissue.Tri[elemi][ni]]+=myTissue.ip_kappaf[elemi];
			//
			node_lamdaP[myTissue.Tri[elemi][ni]]+=myTissue.ip_lamdaP[elemi];
			node_ip_count[myTissue.Tri[elemi][ni]] += 1;
		}
	}
	for(int nodei=0;nodei<myTissue.n_node;nodei++){
		node_phic[nodei] = node_phic[nodei]/node_ip_count[nodei];
		node_mu[nodei] = node_mu[nodei]/node_ip_count[nodei];
		node_kc[nodei] = node_kc[nodei]/node_ip_count[nodei];
		node_a0c[nodei] = node_a0c[nodei]/node_ip_count[nodei];
		node_kappac[nodei] = node_kappac[nodei]/node_ip_count[nodei];
		//
		node_phif[nodei] = node_phif[nodei]/node_ip_count[nodei];
		node_kf[nodei] = node_kf[nodei]/node_ip_count[nodei];
		node_a0f[nodei] = node_a0f[nodei]/node_ip_count[nodei];
		node_kappaf[nodei] = node_kappaf[nodei]/node_ip_count[nodei];
		//
		node_lamdaP[nodei] = node_lamdaP[nodei]/node_ip_count[nodei];
	}
	// rho, c, phi, theta
	if(FLAG==0){
		savefile<<"POINT_DATA "<<myTissue.n_node<<"\nSCALARS rho_c_phic_lamdaP float "<<4<<"\nLOOKUP_TABLE default\n";
		for(int i=0;i<myTissue.n_node;i++){
			savefile<<myTissue.node_rho[i]<<" "<<myTissue.node_c[i]<<" "<<node_phic[i]<<" "<<node_lamdaP[i](0)*node_lamdaP[i](1)<<"\n";
		}
		// save integration point data? usually could be done at the cells
	}
	// else if(FLAG==1){
	// 	savefile<<"POINT_DATA "<<myTissue.n_node<<"\nSCALARS Eu1_Eu2_woundC float "<<3<<"\nLOOKUP_TABLE default\n";
	// 	for(int i=0;i<myTissue.n_node;i++){
	// 		savefile<<myTissue.node_rho[i]<<" "<<myTissue.node_c[i]<<" "<<node_phic[i]<<" "<<node_lamdaP[i](0)*node_lamdaP[i](1)<<"\n";
	// 	}
	// }
	// write out the displacements as node data
	savefile<<"VECTORS u float\n";
	for(int i=0;i<myTissue.n_node;i++){
		savefile<<myTissue.node_x[i](0)-myTissue.node_X[i](0)<<" "<<myTissue.node_x[i](1)-myTissue.node_X[i](1)<<" 0\n";
	}
	// write out cell data
	savefile<<"CELL_DATA "<<myTissue.Tri.size()<<"\nSCALARS phic_EEeiv0_SSeiv0 float "<<3<<"\nLOOKUP_TABLE default\n";
	// declare some variables before
	std::vector<Vector2d> node_x_ni;
	// concentration and cells for previous and current time
	std::vector<double> node_rho_0_ni;
	std::vector<double> node_c_0_ni;
	// values of the structural variables at the IP
	double ip_phic_0_pi;
	double ip_mu_0_pi;
	double ip_kc_0_pi;
	Vector2d ip_a0c_0_pi;
	double ip_kappac_0_pi;
	double ip_phif_0_pi;
	double ip_kf_0_pi;
	Vector2d ip_a0f_0_pi;
	double ip_kappaf_0_pi;
	Vector2d ip_lamdaP_0_pi;
	// OUTPUT VECTORS
	Vector2d EE_eiv,SS_eiv;
	//
	// loop over triangles
	for(int ei=0;ei<myTissue.Tri.size();ei++){
		// calculate strains, call the function
		std::vector<int> elem_ei = myTissue.Tri[ei];
		// nodal positions for this element
		node_x_ni.clear();
		// concentration and cells for previous and current time
		node_rho_0_ni.clear();
		node_c_0_ni.clear();
		//
		for(int ni=0;ni<3;ni++){
			// deformed positions
			node_x_ni.push_back(myTissue.node_x[elem_ei[ni]]);
			// cells and chemical
			node_rho_0_ni.push_back(myTissue.node_rho_0[elem_ei[ni]]);
			node_c_0_ni.push_back(myTissue.node_c_0[elem_ei[ni]]);
		}
		// single IP data
		ip_phic_0_pi = myTissue.ip_phic_0[ei];
		ip_mu_0_pi= myTissue.ip_mu_0[ei];
		ip_kc_0_pi= myTissue.ip_kc_0[ei];
		ip_a0c_0_pi= myTissue.ip_a0c_0[ei];
		ip_kappac_0_pi= myTissue.ip_kappac_0[ei];
		//
		ip_phif_0_pi = myTissue.ip_phif_0[ei];
		ip_kf_0_pi= myTissue.ip_kf_0[ei];
		ip_a0f_0_pi= myTissue.ip_a0f_0[ei];
		ip_kappaf_0_pi= myTissue.ip_kappaf_0[ei];
		//
		ip_lamdaP_0_pi= myTissue.ip_lamdaP_0[ei];
		Matrix2d FF, sigma;
		
            	evalStrainsStress(
					myTissue.elem_jac_IP[ei],
					myTissue.global_parameters,myTissue.local_parameters,
					node_rho_0_ni,node_c_0_ni,
					ip_phic_0_pi,ip_mu_0_pi,ip_kc_0_pi,ip_a0c_0_pi,ip_kappac_0_pi,
					ip_phif_0_pi,ip_kf_0_pi,ip_a0f_0_pi,ip_kappaf_0_pi,
					ip_lamdaP_0_pi,
					node_x_ni,
					EE_eiv, SS_eiv, FF, sigma);

		savefile<<myTissue.ip_phic[ei]<<" "<<EE_eiv(0)<<" "<<SS_eiv(0)<<"\n";
	}

	savefile.close();
}


//---------------------------------------//
// write NODE data to a file
//---------------------------------------//
void writeNode(tissue &myTissue,const char* filename,int nodei,double time)
{
	// write node i to a file
	std::ofstream savefile;
	savefile.open(filename, std::ios_base::app);
	savefile<< time<<","<<myTissue.node_x[nodei](0)<<","<<myTissue.node_x[nodei](1)<<","<<myTissue.node_rho[nodei]<<","<<myTissue.node_c[nodei]<<"\n";
	savefile.close();
}

//---------------------------------------//
// write Integration Point data to a file
//---------------------------------------//
void writeIP(tissue &myTissue,const char* filename,int ipi,double time)
{
	// write integration point i to a file
	std::ofstream savefile;
	savefile.open(filename, std::ios_base::app);
	savefile<< time<<","<<myTissue.ip_phic[ipi]<<","<<myTissue.ip_mu[ipi]<<myTissue.ip_kc[ipi]<<myTissue.ip_a0c[ipi](0)<<","<<myTissue.ip_a0c[ipi](1)<<","<<myTissue.ip_kappac[ipi]<<","<<myTissue.ip_phif[ipi]<<","<<myTissue.ip_kf[ipi]<<myTissue.ip_a0f[ipi](0)<<","<<myTissue.ip_a0f[ipi](1)<<","<<myTissue.ip_kappaf[ipi]<<","<<myTissue.ip_lamdaP[ipi](0)<<","<<myTissue.ip_lamdaP[ipi](1)<<"\n";
	savefile.close();
}

//---------------------------------------//
// write Element data to a file
//---------------------------------------//
void writeElement(tissue &myTissue,const char* filename,int elemi,double time)
{
	// write element i to a file
	std::ofstream savefile;
	savefile.open(filename, std::ios_base::app);
	// average the nodes and the integration points of this element
	std::vector<int> element = myTissue.Tri[elemi];
	double rho=0;
	double c = 0;
	Vector2d x;x.setZero();
	double phic = myTissue.ip_phic[elemi];
	double	mu = myTissue.ip_mu[elemi];
	double	kc = myTissue.ip_kc[elemi];
	Vector2d	a0c = myTissue.ip_a0c[elemi];
	double	kappac = myTissue.ip_kappac[elemi];
	double	phif = myTissue.ip_phif[elemi];
	double	kf = myTissue.ip_kf[elemi];
	Vector2d	a0f = myTissue.ip_a0f[elemi];
	double	kappaf = myTissue.ip_kappaf[elemi];
	Vector2d	lamdaP = myTissue.ip_lamdaP[elemi];
	for(int i=0;i<3;i++){
		x += myTissue.node_x[element[i]];
		rho += myTissue.node_rho[element[i]];
		c += myTissue.node_c[element[i]];
	}
	x = x/3.; rho = rho/3.; c = c/3.;

	savefile<<time<<","<<phic<<","<<kc<<","<<a0c(0)<<","<<a0c(1)<<","<<kappac<<","<<phif<<","<<kf<<","<<a0f(0)<<","<<a0f(1)<<","<<kappaf<<","<<lamdaP(0)<<","<<lamdaP(1)<<","<<rho<<","<<c<<"\n";
}

void projectCC2nodes(const tissue &myTissue, VectorXd &CCnodes_consistent, VectorXd &CCnodes_lumped){
	// Loop over elements and assemble a residual with F and a mass matrix, then project F to nodes
	int n_node = myTissue.node_X.size();
    VectorXd RR(n_node*3); RR.setZero();
	//VectorXd SOL_consistent(n_node*3);SOL_consistent.setZero();
	//VectorXd SOL_lumped(n_node*3);SOL_lumped.setZero();
	CCnodes_consistent.setZero();
	CCnodes_lumped.setZero();
	VectorXd MM_lumped(n_node*3);MM_lumped.setZero();
    //SpMat KK(n_dof,n_dof); // sparse to solve with BiCG
    SparseMatrix<double, ColMajor> MM_consistent(n_node*3,n_node*3);MM_consistent.setZero();
	std::vector<T> MM_triplets;MM_triplets.clear();
	SparseLU<SparseMatrix<double, ColMajor>, COLAMDOrdering<int> >   solver;
	
	//
	//------------------------------------//
		
	// consistent mass matrix for the CST
	MatrixXd MMe(18,18); MMe.setZero();
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			for(int k=0;k<3;k++){
				MMe(i*3+k,j*3+k)= 1/12.0; 
				if(i==j){
					MMe(i*3+k,j*3+k)= 1/6.0; 	
				}
			}
		}
	}

	// START LOOP OVER ELEMENTS
	int n_elem = myTissue.Tri.size();
	for(int ei=0;ei<n_elem;ei++)
	{
	  	// element stuff
	  	Matrix2d Jac = myTissue.elem_jac_IP[ei];
	  	double Jac_det = Jac.determinant();
		std::vector<Vector2d> dRdXi;dRdXi.clear();
		Vector2d dxdxi,dxdeta;
		dxdxi.setZero();dxdeta.setZero();
		double xi  = 1./3.;
  		double eta = 1./3.;
  		std::vector<double> Rxi ={-1.,1.,0.};
  		std::vector<double> Reta={-1.,0.,1.};
		for(int ni=0;ni<3;ni++){
			dRdXi.push_back(Vector2d(Rxi[ni],Reta[ni]));
			dxdxi += myTissue.node_x[myTissue.Tri[ei][ni]]*Rxi[ni];
			dxdeta += myTissue.node_x[myTissue.Tri[ei][ni]]*Reta[ni];
		}
		Matrix2d dxdXi; dxdXi<<dxdxi(0),dxdeta(0),dxdxi(1),dxdeta(1);
		// F = dxdX
		Matrix2d FF = dxdXi*Jac.transpose();
		Matrix2d CC = FF.transpose()*FF;
		Vector3d CCvoigt; CCvoigt<<CC(0,0),CC(1,1),CC(0,1);
		// LOOP OVER NODES
		for(int nodei=0;nodei<3;nodei++){
			// ASSEMBLE RESIDUAL AND TANGENTS
			for(int coordi=0;coordi<3;coordi++){		
				// residual
				RR(myTissue.Tri[ei][nodei]*3+coordi) += (1/3.)*CCvoigt(coordi)*Jac_det/2.0;
				// loop over nodes for consistent mass matrix
				for(int nodej=0;nodej<3;nodej++){
					T MM_nici_njcj = {myTissue.Tri[ei][nodei]*3+coordi,myTissue.Tri[ei][nodej]*3+coordi,MMe(nodei*3+coordi,nodej*3+coordi)*Jac_det/2.0};
					MM_triplets.push_back(MM_nici_njcj);
				}
				// lumped mass matrix
				MM_lumped(myTissue.Tri[ei][nodei]*3+coordi) += (1/3.)*Jac_det/2.0;
			}
			
		}// FINISH LOOP OVER NODES (for assembly)
	}// FINISH LOOP OVER ELEMENTS

	MM_consistent.setFromTriplets(MM_triplets.begin(), MM_triplets.end());
	MM_consistent.makeCompressed();
	solver.compute(MM_consistent);
	//Use the factors to solve the linear system
	//SOL_consistent = solver.solve(RR);
	CCnodes_consistent = solver.solve(RR);
	for(int ni=0;ni<myTissue.node_X.size();ni++){
		for(int ci=0;ci<3;ci++){
			CCnodes_lumped(ni*3+ci) = RR(ni*3+ci)/MM_lumped(ni*3+ci);
		}
	}
}