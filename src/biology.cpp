/*
	Constitutive Equations

	For the biology
*/
//
//------------------------------------------------------------------------------------//
// NOTES:
// * we use \kappa_f to distinguish between wounded and non-wounded region
// * the evolution equation for \phi_f^W affects only the wound region (i.e. where \kappa_f = 1)
// * the evolution equation for \phi_c affects each and every element in the model
// * we use k_f = \xi_c to model evolution of collagen crosslinks, and kc is set based on \phi_c and \xi_c
// * kc does not change here, since we assume that kc = k_1 = \hat{k_1} * \xi_c^exp_csi * \phi_c with fixed \hat{k_1} and exp_csi
//------------------------------------------------------------------------------------//
//
#include "biology.h"
#include <vector>
#include <map>
#include <math.h>
#include <algorithm>
#include <iostream>
#include <Eigen/Dense> // most of the vector functions I will need inside of an element
using namespace Eigen;

//------------------------------------------------------------------------------------//
// 	biology of global variables
//------------------------------------------------------------------------------------//

// for the flux function
// This defines the active (2nd PK) stress term, i.e. the one due to fibroblast contraction
Matrix2d evalSS_act(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double> &global_parameters, double load )
{

	// Parameters
	double t_rho = global_parameters[3];
	double t_rho_c = global_parameters[4];
	double K_t_c = global_parameters[5];

	// Preprocess
	Matrix2d Identity;Identity<<1.0,0.0,0.0,1.0;
	Matrix2d a0ca0c = a0c*a0c.transpose();
	Matrix2d A0c = kappac*Identity + (1.0-3.0*kappac)*a0ca0c;
	double thetaP = lamdaP(0)*lamdaP(1);
	Vector2d ac = FF*a0c;
	Matrix2d Ac = kappac*FF*FF.transpose() + (1.0-3.0*kappac)*ac*ac.transpose();
	double trAc = Ac(0,0) + Ac(1,1);

	// Calculate active stress
	double traction_act = (t_rho + t_rho_c*c/(K_t_c + c))*rho;
	Matrix2d SS_act = thetaP*(traction_act*phic/trAc)*A0c;

	return SS_act;

}


// for the tangent function
// This computes the numerical tangent of the active stress term
void evalSS_act(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double phic, double kc, const Vector2d &a0c, double kappac,
	double phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP,  const std::vector<double> &global_parameters, double load,
	Matrix2d &SS_act, Matrix3d &DDact, Matrix2d &dSS_actdrho_explicit, Matrix2d &dSS_actdc_explicit)
{

	SS_act = evalSS_act(FF, rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);

	// Mechanics
	// numerical
	Matrix2d CC = FF.transpose()*FF;
	EigenSolver<Matrix2d> es(CC);
	// then polar decomposition is
	std::vector<Vector2d> Ubasis(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv = es.eigenvectors().real();
	Ubasis[0] =  CC_eiv.col(0);
	Ubasis[1] =  CC_eiv.col(1);
	Vector2d Clamda =  es.eigenvalues().real();
	// Build the U matrix from basis and the root of the eigenvuales
	Matrix2d UU = sqrt(Clamda(0))*Ubasis[0]*Ubasis[0].transpose() + sqrt(Clamda(1))*Ubasis[1]*Ubasis[1].transpose() ;
	Matrix2d RR = FF*UU.inverse();
	Matrix2d FF_plus,FF_minus;
	Matrix2d CC_plus,CC_minus;
	Matrix2d UU_plus,UU_minus;
	std::vector<Vector2d> Ebasis(2,Vector2d(0.0,0.0));
	Ebasis[0] = Vector2d(1.0,0.0);
	Ebasis[1] = Vector2d(0.0,1.0);
	std::vector<Vector2d> Ubasis_plus(2,Vector2d(0.0,0.0));
	std::vector<Vector2d> Ubasis_minus(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv_plus,CC_eiv_minus;
	Vector2d Clamda_plus,Clamda_minus;
	//
	Matrix2d SSact_plus,SSact_minus;
	//
	double epsilon = 1e-8;
	//
	Vector3d voigt_table_I_i(0,1,0);
	Vector3d voigt_table_I_j(0,1,1);
	Vector3d voigt_table_J_k(0,1,0);
	Vector3d voigt_table_J_l(0,1,1);
	int ii,jj,kk,ll;
	for(int II=0;II<3;II++){
		for(int JJ=0;JJ<3;JJ++){
			ii = voigt_table_I_i(II);
			jj = voigt_table_I_j(II);
			kk = voigt_table_J_k(JJ);
			ll = voigt_table_J_l(JJ);

			CC_plus = CC + epsilon*(Ebasis[kk]*Ebasis[ll].transpose())+ epsilon*(Ebasis[ll]*Ebasis[kk].transpose());
			CC_minus = CC - epsilon*(Ebasis[kk]*Ebasis[ll].transpose())- epsilon*(Ebasis[ll]*Ebasis[kk].transpose());

			// Polar decomposition of CCplus and CCminus
			EigenSolver<Matrix2d> esp(CC_plus);
			EigenSolver<Matrix2d> esm(CC_minus);
			Matrix2d CC_eiv_plus = esp.eigenvectors().real();
			Matrix2d CC_eiv_minus = esm.eigenvectors().real();
			Ubasis_plus[0] =  CC_eiv_plus.col(0);
			Ubasis_plus[1] =  CC_eiv_plus.col(1);
			Ubasis_minus[0] =  CC_eiv_minus.col(0);
			Ubasis_minus[1] =  CC_eiv_minus.col(1);
			Clamda_plus =  esp.eigenvalues().real();
			Clamda_minus =  esm.eigenvalues().real();
			UU_plus = sqrt(Clamda_plus(0))*Ubasis_plus[0]*Ubasis_plus[0].transpose() + sqrt(Clamda_plus(1))*Ubasis_plus[1]*Ubasis_plus[1].transpose() ;
			UU_minus = sqrt(Clamda_minus(0))*Ubasis_minus[0]*Ubasis_minus[0].transpose() + sqrt(Clamda_minus(1))*Ubasis_minus[1]*Ubasis_minus[1].transpose() ;
			FF_plus = RR*UU_plus;
			FF_minus = RR*UU_minus;

			SSact_plus =  evalSS_act(FF_plus,   rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);
			SSact_minus =  evalSS_act(FF_minus, rho, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);

			DDact(II,JJ) =  (1.0/(4.0*epsilon))*( SSact_plus(ii,jj) - SSact_minus(ii,jj) );
		}
	}


	// rho
	double rho_plus = rho+epsilon;
	double rho_minus = rho-epsilon;
	SSact_plus =  evalSS_act(FF,  rho_plus,  Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);
	SSact_minus =  evalSS_act(FF, rho_minus, Grad_rho, c, Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);
	//
	dSS_actdrho_explicit = (1/(2*epsilon))*(SSact_plus-SSact_minus);

	// c
	double c_plus = c+epsilon;
	double c_minus = c-epsilon;
	SSact_plus =  evalSS_act(FF,rho,  Grad_rho, c_plus,  Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);
	SSact_minus =  evalSS_act(FF,rho, Grad_rho, c_minus, Grad_c, phic, kc, a0c, kappac, phif, kf, a0f, kappaf, lamdaP, global_parameters, load);
	//
	dSS_actdc_explicit =(1/(2*epsilon))*(SSact_plus-SSact_minus);

}

// Eval flux and source terms for the cell and the chemical

Vector2d evalQ_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>& global_parameters)
{

	// parameters
	double D_rhorho = global_parameters[6];
	double D_rhoc = global_parameters[7];

	// Pre-process
	double thetaP = lamdaP(0)*lamdaP(1);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d FFinv = FF.inverse();
	Matrix2d CCinv = CC.inverse();
	Matrix2d Identity; Identity<<1.0,0.0,0.0,1.0;

	// turn diffusion coefficients into diagonal matrices
	Matrix2d DD_rhorho = D_rhorho*Identity;
	Matrix2d DD_rhoc = D_rhoc*Identity;

	// flux vector 
	Vector2d Q_rho = - thetaP*FFinv*DD_rhorho*FFinv.transpose()*Grad_rho - thetaP*FFinv*DD_rhoc*FFinv.transpose()*Grad_c;

	return Q_rho;

}


// Flux of rho and Tangent
void  evalQ_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>&global_parameters,
	Vector2d &Q_rho, Matrix2d &dQ_rhoxdCC_explicit, Matrix2d &dQ_rhoydCC_explicit, Vector2d &dQ_rhodrho_explicit, Matrix2d &dQ_rhodGradrho, Vector2d &dQ_rhodc_explicit, Matrix2d &dQ_rhodGradc)
{

	// Parameters
	double D_rhorho = global_parameters[6];
	double D_rhoc = global_parameters[7];

	// Pre-process
	double thetaP = lamdaP(0)*lamdaP(1);
	Matrix2d FFinv = FF.inverse();
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	Matrix2d Identity; Identity<<1.0,0.0,0.0,1.0;

	// turn diffusion coefficients into diagonal matrices
	Matrix2d DD_rhorho = D_rhorho*Identity;
	Matrix2d DD_rhoc = D_rhoc*Identity;

	// flux
	Q_rho = - thetaP*FFinv*DD_rhorho*FFinv.transpose()*Grad_rho - thetaP*FFinv*DD_rhoc*FFinv.transpose()*Grad_c;

	// Tangents

	// Mechanics
	// numerical
	EigenSolver<Matrix2d> es(CC);
	// then polar decomposition is
	std::vector<Vector2d> Ubasis(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv = es.eigenvectors().real();
	Ubasis[0] =  CC_eiv.col(0);
	Ubasis[1] =  CC_eiv.col(1);
	Vector2d Clamda =  es.eigenvalues().real();
	// Build the U matrix from basis and the root of the eigenvuales
	Matrix2d UU = sqrt(Clamda(0))*Ubasis[0]*Ubasis[0].transpose() + sqrt(Clamda(1))*Ubasis[1]*Ubasis[1].transpose() ;
	Matrix2d RR = FF*UU.inverse();
	Matrix2d FF_plus,FF_minus;
	Matrix2d CC_plus,CC_minus;
	Matrix2d UU_plus,UU_minus;
	std::vector<Vector2d> Ebasis(2,Vector2d(0.0,0.0));
	Ebasis[0] = Vector2d(1.0,0.0);
	Ebasis[1] = Vector2d(0.0,1.0);
	std::vector<Vector2d> Ubasis_plus(2,Vector2d(0.0,0.0));
	std::vector<Vector2d> Ubasis_minus(2,Vector2d(0.0,0.0));
	Matrix2d CC_eiv_plus,CC_eiv_minus;
	Vector2d Clamda_plus,Clamda_minus;
	Vector2d Qrho_pFF, Qrho_mFF;
	double epsilon = 1e-7;
	//
	for(int coordk = 0;coordk<2;coordk++){
		for(int coordl = coordk;coordl<2;coordl++){
			CC_plus = CC + epsilon*(Ebasis[coordk]*Ebasis[coordl].transpose())+ epsilon*(Ebasis[coordl]*Ebasis[coordk].transpose());
			CC_minus = CC - epsilon*(Ebasis[coordk]*Ebasis[coordl].transpose())- epsilon*(Ebasis[coordl]*Ebasis[coordk].transpose());
			// Polar decomposition of CCplus and CCminus
			EigenSolver<Matrix2d> esp(CC_plus);
			EigenSolver<Matrix2d> esm(CC_minus);
			Matrix2d CC_eiv_plus = esp.eigenvectors().real();
			Matrix2d CC_eiv_minus = esm.eigenvectors().real();
			Ubasis_plus[0] =  CC_eiv_plus.col(0);
			Ubasis_plus[1] =  CC_eiv_plus.col(1);
			Ubasis_minus[0] =  CC_eiv_minus.col(0);
			Ubasis_minus[1] =  CC_eiv_minus.col(1);
			Clamda_plus =  esp.eigenvalues().real();
			Clamda_minus =  esm.eigenvalues().real();
			UU_plus = sqrt(Clamda_plus(0))*Ubasis_plus[0]*Ubasis_plus[0].transpose() + sqrt(Clamda_plus(1))*Ubasis_plus[1]*Ubasis_plus[1].transpose() ;
			UU_minus = sqrt(Clamda_minus(0))*Ubasis_minus[0]*Ubasis_minus[0].transpose() + sqrt(Clamda_minus(1))*Ubasis_minus[1]*Ubasis_minus[1].transpose() ;
			FF_plus = RR*UU_plus;
			FF_minus = RR*UU_minus;
			Qrho_pFF = evalQ_rho(FF_plus,  rho,Grad_rho, c, Grad_c,  Phic, kc, a0c, kappac, Phif, kf, a0f, kappaf, lamdaP, global_parameters);
			Qrho_mFF = evalQ_rho(FF_minus, rho,Grad_rho, c, Grad_c,  Phic, kc, a0c, kappac, Phif, kf, a0f, kappaf, lamdaP, global_parameters);
			dQ_rhoxdCC_explicit(coordk,coordl) = (1.0/(4.0*epsilon))*(Qrho_pFF(0) - Qrho_mFF(0) );
			dQ_rhoxdCC_explicit(coordl,coordk) = (1.0/(4.0*epsilon))*(Qrho_pFF(0) - Qrho_mFF(0) );
			dQ_rhoydCC_explicit(coordk,coordl) = (1.0/(4.0*epsilon))*(Qrho_pFF(1) - Qrho_mFF(1) );
			dQ_rhoydCC_explicit(coordl,coordk) = (1.0/(4.0*epsilon))*(Qrho_pFF(1) - Qrho_mFF(1) );
		}
	}

	// rho
	double rho_plus = rho+epsilon;
	double rho_minus = rho-epsilon;

	Vector2d Qrho_pl = evalQ_rho(FF, rho_plus,  Grad_rho, c, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Vector2d Qrho_mi = evalQ_rho(FF, rho_minus, Grad_rho, c, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	dQ_rhodrho_explicit = (1.0/(2.0*epsilon))*(Qrho_pl - Qrho_mi);

	// Gradrho
	Vector2d Grad_rho_px = Grad_rho + epsilon*Ebasis[0];
	Vector2d Grad_rho_py = Grad_rho + epsilon*Ebasis[1];
	Vector2d Grad_rho_mx = Grad_rho - epsilon*Ebasis[0];
	Vector2d Grad_rho_my = Grad_rho - epsilon*Ebasis[1];

	// Q_rho
	Vector2d Qrho_plx = evalQ_rho(FF, rho, Grad_rho_px, c, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Vector2d Qrho_mix = evalQ_rho(FF, rho, Grad_rho_mx, c, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Vector2d Qrho_ply = evalQ_rho(FF, rho, Grad_rho_py, c, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Vector2d Qrho_miy = evalQ_rho(FF, rho, Grad_rho_my, c, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	dQ_rhodGradrho.col(0)= (1.0/(2.0*epsilon))*(Qrho_plx-Qrho_mix);
	dQ_rhodGradrho.col(1)= (1.0/(2.0*epsilon))*(Qrho_ply-Qrho_miy);

	// c
	double c_plus = c+epsilon;
	double c_minus = c-epsilon;
	Qrho_pl = evalQ_rho(FF, rho, Grad_rho, c_plus,  Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Qrho_mi = evalQ_rho(FF, rho, Grad_rho, c_minus, Grad_c,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	dQ_rhodc_explicit = (1.0/(2.0*epsilon))*(Qrho_pl - Qrho_mi);

	// Gradc
	Vector2d Grad_c_px = Grad_c + epsilon*Ebasis[0];
	Vector2d Grad_c_py = Grad_c + epsilon*Ebasis[1];
	Vector2d Grad_c_mx = Grad_c - epsilon*Ebasis[0];
	Vector2d Grad_c_my = Grad_c - epsilon*Ebasis[1];

	// Q_rho
	Qrho_plx = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_px,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Qrho_mix = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_mx,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Qrho_ply = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_py,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	Qrho_miy = evalQ_rho(FF, rho, Grad_rho, c, Grad_c_my,  Phic,  kc,  a0c,  kappac, Phif,  kf,  a0f,  kappaf, lamdaP, global_parameters);
	dQ_rhodGradc.col(0)= (1.0/(2.0*epsilon))*(Qrho_plx-Qrho_mix);
	dQ_rhodGradc.col(1)= (1.0/(2.0*epsilon))*(Qrho_ply-Qrho_miy);

}

// Flux of chemical
Vector2d evalQ_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>& global_parameters)
{

	// Parameters
	double D_cc = global_parameters[9];

	// Preprocessing
	double thetaP = lamdaP(0)*lamdaP(1);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();

	Vector2d Q_c = -thetaP*D_cc*CCinv*Grad_c;

	return Q_c;

}

// flux and tangent of chemical
void  evalQ_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>&global_parameters,
	Vector2d &Q_c, Matrix2d &dQ_cxdCC_explicit, Matrix2d &dQ_cydCC_explicit, Vector2d &dQ_cdrho_explicit, Matrix2d &dQ_cdGradrho, Vector2d &dQ_cdc_explicit, Matrix2d &dQ_cdGradc)
{

	// Parameters
	double D_cc = global_parameters[9];

	// Preprocessing
	double thetaP = lamdaP(0)*lamdaP(1);
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();

	// Flux
	Q_c = -thetaP*D_cc*CCinv*Grad_c;

	// Tangents

	// Mechanics
	dQ_cxdCC_explicit.setZero();
	dQ_cydCC_explicit.setZero();
	double dCCinvdCC_x;
	double dCCinvdCC_y;
	for(int kk=0;kk<2;kk++){
		for(int ll=0;ll<2;ll++){
			for(int jj=0;jj<2;jj++){
				dCCinvdCC_x = (-0.5*(CCinv(0,kk)*CCinv(jj,ll)+CCinv(0,ll)*CCinv(jj,kk)));
				dCCinvdCC_y = (-0.5*(CCinv(1,kk)*CCinv(jj,ll)+CCinv(1,ll)*CCinv(jj,kk)));
				dQ_cxdCC_explicit(kk,ll) += -D_cc*dCCinvdCC_x*Grad_c(jj);
				dQ_cydCC_explicit(kk,ll) += -D_cc*dCCinvdCC_y*Grad_c(jj);
			}
		}
	}
	// rho
	dQ_cdrho_explicit.setZero();

	// Grad_rho
	dQ_cdGradrho.setZero();

	// c
	dQ_cdc_explicit.setZero();

	//Grad_c
	dQ_cdGradc = -thetaP*D_cc*CCinv;

}


// source of rho
double evalS_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>&global_parameters)
{

	// Parameters
	double p_rho = global_parameters[10];
	double p_rho_c = global_parameters[11];
	double p_rho_thetaE = global_parameters[12];
	double K_rho_c = global_parameters[13];
	double K_rho_rho = global_parameters[14];
	double d_rho = global_parameters[15];
	double theta_phy = global_parameters[16];
	double gamma_c_thetaE = global_parameters[17];

	// Pre-process
	double theta = FF.determinant();
	double thetaP = lamdaP(0)*lamdaP(1);
	double thetaE = theta/thetaP;

	double He = 1. / (1. + exp(-gamma_c_thetaE * (thetaE - theta_phy)));

	double S_rho = (p_rho + p_rho_c * c / (K_rho_c + c) + p_rho_thetaE * He) * (1. - rho / K_rho_rho) * rho - d_rho * rho;

	return S_rho;

}

// source of rho for tangent
void evalS_rho(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>&global_parameters,
	double &S_rho,Matrix2d &dSrhodCC_explicit,double &dSrhodrho_explicit, double &dSrhodc_explicit)
{

	// Parameters
	double p_rho = global_parameters[10];
	double p_rho_c = global_parameters[11];
	double p_rho_thetaE = global_parameters[12];
	double K_rho_c = global_parameters[13];
	double K_rho_rho = global_parameters[14];
	double d_rho = global_parameters[15];
	double theta_phy = global_parameters[16];
	double gamma_c_thetaE = global_parameters[17];

	// Pre-process
	double theta = FF.determinant();
	double thetaP = lamdaP(0)*lamdaP(1);
	double thetaE = theta/thetaP;
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	Matrix2d dthetadCC = 0.5*theta*CCinv;

	double He = 1. / (1. + exp(-gamma_c_thetaE * (thetaE - theta_phy)));

	// Source
	S_rho = (p_rho + p_rho_c * c / (K_rho_c + c) + p_rho_thetaE * He) * (1. - rho / K_rho_rho) * rho - d_rho * rho;

	// Derivatives

	// Mechanics
	Matrix2d dHedCC_explicit = gamma_c_thetaE * exp(-gamma_c_thetaE*(thetaE - theta_phy)) * He * He * dthetadCC;
	
	dSrhodCC_explicit = p_rho_thetaE * (1. - rho / K_rho_rho) * rho * dHedCC_explicit;

	// rho
	dSrhodrho_explicit = ( p_rho + p_rho_c * c / (K_rho_c + c) + p_rho_thetaE * He ) * ( 1.0 - 2.0 * rho / K_rho_rho) - d_rho;

	// c
	dSrhodc_explicit = p_rho_c * (1.0 - rho / K_rho_rho) * rho * K_rho_c / (K_rho_c + c) / (K_rho_c + c);

}

// source of chemical
double evalS_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, double prec,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>& global_parameters)
{

	// Parameters
	double theta_phy = global_parameters[16];
	double gamma_c_thetaE = global_parameters[17];
	double p_c_rho = global_parameters[18];
	double p_c_thetaE = global_parameters[19];
	double K_c_c = global_parameters[20];
	double d_c = global_parameters[21];
	double p_c_prec = global_parameters[25];
	double c_target = global_parameters[27];

	// Pre-process
	double theta = FF.determinant();
	double thetaP = lamdaP(0)*lamdaP(1);
	double thetaE = theta/thetaP;

	// Source
	double He = 1. / (1. + exp(-gamma_c_thetaE * (thetaE - theta_phy)));

	double S_c;

	// distinguish case due to MacAuley brackets on (c_target - c)
	if ( c < c_target )
		S_c = p_c_prec * prec + (p_c_rho * (c_target - c) + p_c_thetaE * He) * (rho / (K_c_c + c)) - d_c * c;
	else
		S_c = p_c_prec * prec + (p_c_thetaE * He) * (rho / (K_c_c + c)) - d_c * c;

	return S_c;

}

// source of the chemical for the tangent
void evalS_c(
	const Matrix2d &FF, double rho, const Vector2d &Grad_rho, double c, const Vector2d &Grad_c, double prec,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif, double kf, const Vector2d &a0f, double kappaf,const Vector2d &lamdaP, const std::vector<double>& global_parameters,
	double &S_c,Matrix2d &dScdCC_explicit,double &dScdrho_explicit, double &dScdc_explicit)
{

	// Parameters
	double theta_phy = global_parameters[16];
	double gamma_c_thetaE = global_parameters[17];
	double p_c_rho = global_parameters[18];
	double p_c_thetaE = global_parameters[19];
	double K_c_c = global_parameters[20];
	double d_c = global_parameters[21];
	double p_c_prec = global_parameters[25];
	double c_target = global_parameters[27];

	// Pre-process
	double theta = FF.determinant();
	double thetaP = lamdaP(0)*lamdaP(1);
	double thetaE = theta/thetaP;
	Matrix2d CC = FF.transpose()*FF;
	Matrix2d CCinv = CC.inverse();
	Matrix2d dthetadCC = 0.5*theta*CCinv;

	// Source
	double He = 1./(1. + exp(-gamma_c_thetaE * (thetaE - theta_phy)));

	// distinguish case due to MacAuley brackets on (c_target - c)
	if ( c < c_target )
		S_c = p_c_prec * prec + (p_c_rho * (c_target - c) + p_c_thetaE * He) * (rho / (K_c_c + c)) - d_c * c;
	else
		S_c = p_c_prec * prec + (p_c_thetaE * He) * (rho / (K_c_c + c)) - d_c * c;

	// Derivatives

	// Mechanics
	Matrix2d dHedCC_explicit = gamma_c_thetaE * exp(-gamma_c_thetaE*(thetaE - theta_phy)) * He * He * dthetadCC;

	dScdCC_explicit = rho / (K_c_c + c) * p_c_thetaE * dHedCC_explicit;

	// rho
	if ( c < c_target )
		dScdrho_explicit = (p_c_rho * (c_target - c) + p_c_thetaE * He) * (1. / (K_c_c + c));
	else
		dScdrho_explicit = (p_c_thetaE * He) * (1. / (K_c_c + c));

	// c
	if ( c < c_target )
		dScdc_explicit = -d_c + ( p_c_rho * ( - 1.0 - (c_target - c) / (K_c_c + c) ) - p_c_thetaE * He / (K_c_c + c) ) * rho / (K_c_c + c);
	else
		dScdc_explicit = -d_c + ( - p_c_thetaE * He / (K_c_c + c) ) * rho / (K_c_c + c);

}


//------------------------------------------------------------------------------------//
// 	biology of local variables
//------------------------------------------------------------------------------------//

// collagen production and degradation
double eval_phic_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double>& local_parameters)
{

	// Parameters
	double p_phi = local_parameters[11];
	double p_phi_c = local_parameters[12];
	double K_phi_c = local_parameters[13];
	double p_phi_theta = local_parameters[14];
	double K_phi_rho = local_parameters[15];
	double theta_phy = local_parameters[18];
	double gamma_c_thetaE = local_parameters[19];

	// Pre-process
	double theta = sqrt(CC.determinant());
	double thetaP = lamdaP(0)*lamdaP(1);
	double thetaE = theta/thetaP;

	// Collagen deposition
	double He = 1. / (1. + exp(-gamma_c_thetaE * (thetaE - theta_phy)));
	double phic_dot_plus = ( p_phi + p_phi_c * c / ( K_phi_c + c ) + p_phi_theta * He ) * rho / ( K_phi_rho + Phic );

	return phic_dot_plus;

}

void  eval_phic_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phic_dot_plus,  Matrix2d &dphicdotplusdCC, double &dphicdotplusdrho, double &dphicdotplusdc)
{
	
	// Parameters
	double p_phi = local_parameters[11];
	double p_phi_c = local_parameters[12];
	double K_phi_c = local_parameters[13];
	double p_phi_theta = local_parameters[14];
	double K_phi_rho = local_parameters[15];
	double theta_phy = local_parameters[18];
	double gamma_c_thetaE = local_parameters[19];
	
	// Pre-process
	double theta = sqrt(CC.determinant());
	double thetaP = lamdaP(0)*lamdaP(1);
	double thetaE = theta/thetaP;
	Matrix2d CCinv = CC.inverse();
	Matrix2d dthetadCC = 0.5*theta*CCinv;

	// Phic dot
	phic_dot_plus = eval_phic_dot_plus(CC, rho, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);

	// Derivatives

	// Mechanics
	double He = 1. / (1. + exp(-gamma_c_thetaE * (thetaE - theta_phy)));
	Matrix2d dHedCC_explicit = gamma_c_thetaE * exp(-gamma_c_thetaE*(thetaE - theta_phy)) * He * He * dthetadCC;
	dphicdotplusdCC = p_phi_theta * rho / ( K_phi_rho + Phic ) * dHedCC_explicit;

	// rho
	double epsilon = 1e-7;
	double rhop = rho + epsilon;
	double rhom = rho - epsilon;
	double phicdotplus_p =  eval_phic_dot_plus(CC, rhop, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	double phicdotplus_m =  eval_phic_dot_plus(CC, rhom, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dphicdotplusdrho = (1.0/(2.0*epsilon))*(phicdotplus_p - phicdotplus_m);

	// c
	double cp = c + epsilon;
	double cm = c - epsilon;
	phicdotplus_p =  eval_phic_dot_plus(CC, rho, cp, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	phicdotplus_m =  eval_phic_dot_plus(CC, rho, cm, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dphicdotplusdc =  (1.0/(2.0*epsilon))*(phicdotplus_p - phicdotplus_m);

}

//
double eval_phic_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters)
{

	// Parameters
	double d_phi = local_parameters[16];
	double d_phi_rho_c = local_parameters[17];

	// Collagen degradation
	double phic_dot_minus =  ( d_phi + c * rho * d_phi_rho_c) * Phic;
	
	return phic_dot_minus;

}

void eval_phic_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phic_dot_minus, Matrix2d &dphicdotminusdCC, double &dphicdotminusdrho, double& dphicdotminusdc)
{

	// Parameters
	double d_phi = local_parameters[16];
	double d_phi_rho_c = local_parameters[17];

	// Collagen degradation
	phic_dot_minus =  eval_phic_dot_minus(CC, rho, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);

	// Derivatives
	
	// Mechanics
	dphicdotminusdCC.setZero();

	// rho
	dphicdotminusdrho = c * d_phi_rho_c * Phic;
	
	// c
	dphicdotminusdc = rho * d_phi_rho_c * Phic;

}

// fibronectin production and degradation
double eval_phif_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters)
{

	double phif_dot_plus = 0.;

	return phif_dot_plus;

}

void eval_phif_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phif_dot_plus,  Matrix2d &dphifdotplusdCC, double &dphifdotplusdrho, double &dphifdotplusdc)
{

	phif_dot_plus = eval_phif_dot_plus(CC, rho, c, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);

	// Mechanics
	dphifdotplusdCC.setZero();

	// rho
	dphifdotplusdrho = 0.;

	// c
	dphifdotplusdc =  0.;

}

//
double eval_phif_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters)
{

	// fibrin degradation
	double d_phi = local_parameters[16];
	double d_phif = local_parameters[23];
	double d_phif_c_rho = local_parameters[24];
	double phif_dot_minus = 0.0;

	if ( kappaf == 1.0 ) // phif decays only in the wound
		phif_dot_minus = ( d_phif + d_phif_c_rho * c * rho ) * Phif;

	return phif_dot_minus;

}

void eval_phif_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &phif_dot_minus, Matrix2d &dphifdotminusdCC, double &dphifdotminusdrho, double& dphifdotminusdc)
{

	// fibrin degradation
	phif_dot_minus = eval_phif_dot_minus(CC, rho, c, prec, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);

	// Derivatives
	// mechanics
	dphifdotminusdCC.setZero();
	// rho
	dphifdotminusdrho = 0.;
	// c
	dphifdotminusdc = 0.;

}

// collagen stiffness changes
double eval_kc_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double>& local_parameters)
{

	// kc does not change
	double kc_dot_plus = 0.0;

	return kc_dot_plus;

}

void eval_kc_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kc_dot_plus,  Matrix2d &dkcdotplusdCC,  double &dkcdotplusdrho, double &dkcdotplusdc)
{

	// kc dot
	kc_dot_plus = eval_kc_dot_plus(CC, rho, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);

	// Derivatives

	// Mechanics
	dkcdotplusdCC.setZero();

	// rho
	double epsilon = 1e-7;
	double rhop = rho + epsilon;
	double rhom = rho - epsilon;
	double kcdotplus_p =  eval_kc_dot_plus(CC, rhop, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	double kcdotplus_m =  eval_kc_dot_plus(CC, rhom, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dkcdotplusdrho = (1.0/(2.0*epsilon))*(kcdotplus_p - kcdotplus_m);

	// c
	double cp = c + epsilon;
	double cm = c - epsilon;
	kcdotplus_p =  eval_kc_dot_plus(CC, rho, cp, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	kcdotplus_m =  eval_kc_dot_plus(CC, rho, cm, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dkcdotplusdc =  (1.0/(2.0*epsilon))*(kcdotplus_p - kcdotplus_m);

}

//
double eval_kc_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters)
{

	// kc does not change
	double kc_dot_minus = 0.0;

	return kc_dot_minus;

}

void eval_kc_dot_minus(
	const Matrix2d &CC,double rho, double c,
	double Phic, double kc, const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kc_dot_minus, Matrix2d &dkcdotminusdCC, double &dkcdotminusdrho, double &dkcdotminusdc)
{

	// kc dot
	kc_dot_minus = eval_kc_dot_minus(CC, rho, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);

	// Derivatives

	// Mechanics
	dkcdotminusdCC.setZero();

	// rho
	double epsilon = 1e-7;
	double rhop = rho + epsilon;
	double rhom = rho - epsilon;
	double kcdotminus_p =  eval_kc_dot_minus(CC, rhop, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	double kcdotminus_m =  eval_kc_dot_minus(CC, rhom, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dkcdotminusdrho = (1.0/(2.0*epsilon))*(kcdotminus_p - kcdotminus_m);

	// c
	double cp = c + epsilon;
	double cm = c - epsilon;
	kcdotminus_p =  eval_kc_dot_minus(CC, rho, cp, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	kcdotminus_m =  eval_kc_dot_minus(CC, rho, cm, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dkcdotminusdc =  (1.0/(2.0*epsilon))*(kcdotminus_p - kcdotminus_m);

}

// NOTE: we use kf to code evolution equation for collagen crosslinks
double eval_kf_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters)
{

	// Parameters
	double p_phi = local_parameters[11];
	double d_phi = local_parameters[16];
	double d_phi_rho_c = local_parameters[17];
	double p_csi_phic = local_parameters[20];
	double K_csi_phic = local_parameters[21];
	double d_csi_phic_m = local_parameters[22];
	double d_csi = p_csi_phic / ( K_csi_phic + 1. ) - d_csi_phic_m * ( d_phi + d_phi_rho_c );
	double Phic_dot_minus =  eval_phic_dot_minus(CC, rho, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	double csi = kf;

	double kf_dot_plus = p_csi_phic / ( K_csi_phic + csi ) * Phic;

	return kf_dot_plus;

}

void eval_kf_dot_plus(
	const Matrix2d &CC,double rho, double c,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kf_dot_plus,  Matrix2d &dkfdotplusdCC,  double &dkfdotplusdrho, double &dkfdotplusdc)
{
	
	// csic dot = kf dot
	kf_dot_plus = eval_kf_dot_plus(CC, rho, c, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);

	dkfdotplusdCC.setZero();

	// rho
	double epsilon = 1e-7;
	double rhop = rho + epsilon;
	double rhom = rho - epsilon;
	double kfdotplus_p =  eval_kf_dot_plus(CC,rhop, c, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);
	double kfdotplus_m =  eval_kf_dot_plus(CC,rhom, c, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);
	dkfdotplusdrho = (1.0/(2.0*epsilon))*(kfdotplus_p - kfdotplus_m);

	// c
	double cp = c + epsilon;
	double cm = c - epsilon;
	kfdotplus_p =  eval_kf_dot_plus(CC,rho, cp, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);
	kfdotplus_m =  eval_kf_dot_plus(CC,rho, cm, Phic, kc,a0c, kappac, Phif, kf,a0f,  kappaf, lamdaP, local_parameters);
	dkfdotplusdc =  (1.0/(2.0*epsilon))*(kfdotplus_p -kfdotplus_m);

}

//
double eval_kf_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters)
{

	// Parameters
	double p_phi = local_parameters[11];
	double d_phi = local_parameters[16];
	double d_phi_rho_c = local_parameters[17];
	double p_csi_phic = local_parameters[20];
	double K_csi_phic = local_parameters[21];
	double d_csi_phic_m = local_parameters[22];
	double d_csi = p_csi_phic / ( K_csi_phic + 1. ) - d_csi_phic_m * ( d_phi + d_phi_rho_c );
	double Phic_dot_minus =  eval_phic_dot_minus(CC, rho, c, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	double csi = kf;

	double kf_dot_minus = d_csi_phic_m * Phic_dot_minus * csi + d_csi * csi;

	return kf_dot_minus;

}

void  eval_kf_dot_minus(
	const Matrix2d &CC,double rho, double c, double prec,
	double Phic,double kc,const Vector2d &a0c, double kappac,
	double Phif,double kf,const Vector2d &a0f, double kappaf, const Vector2d &lamdaP, const std::vector<double> &local_parameters,
	double &kf_dot_minus, Matrix2d &dkfdotminusdCC, double &dkfdotminusdrho, double &dkfdotminusdc)
{

	// csic dot = kf dot
	kf_dot_minus = eval_kf_dot_minus(CC, rho, c, prec, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);

	// Derivatives

	// Mechanics
	dkfdotminusdCC.setZero();

	// rho
	double epsilon = 1e-7;
	double rhop = rho + epsilon;
	double rhom = rho - epsilon;
	double kfdotminus_p =  eval_kf_dot_minus(CC, rhop, c, prec, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	double kfdotminus_m =  eval_kf_dot_minus(CC, rhom, c, prec, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dkfdotminusdrho = (1.0/(2.0*epsilon))*(kfdotminus_p - kfdotminus_m);

	// c
	double cp = c + epsilon;
	double cm = c - epsilon;
	kfdotminus_p =  eval_kf_dot_minus(CC, rho, cp, prec, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	kfdotminus_m =  eval_kf_dot_minus(CC, rho, cm, prec, Phic, kc, a0c, kappac, Phif, kf, a0f,  kappaf, lamdaP, local_parameters);
	dkfdotminusdc =  (1.0/(2.0*epsilon))*(kfdotminus_p - kfdotminus_m);

}